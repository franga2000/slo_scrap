import importlib
import logging
import time
from collections import defaultdict
from datetime import timedelta
from types import TracebackType

import uvicorn
from fastapi import FastAPI
from fastapi_cache import FastAPICache
from fastapi_cache.backends.inmemory import InMemoryBackend
from fastapi.middleware.cors import CORSMiddleware
from prometheus_fastapi_instrumentator import Instrumentator
from starlette.requests import Request
from starlette.responses import Response

from slo_scrap_api import config

from slo_scrap_api.util import DummyCacheBackend


########################
#   SITE MODULE LIST   #
########################

OJPP_SITES = ('ap_ljubljana', 'apms', 'arriva', 'avant2go', 'greengo', 'ijpp', 'jcdecaux', 'marprom', 'micikel', 'nextbike', 'nomago', 'prevozi', 'scbikes', 'sharengo', 'urbanaplus', 'zapeljime', 'sz', )

OTHER_SITES = ('bolha', 'avto', 'esodstvo', 'hi3', 'prehrana', 'ajpes', 'private', '3rd_party', 'triglav', 'wiz', 'posta', )


###############
#   LOGGING   #
###############

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger("rest_api")


##############
#   SENTRY   #
##############

if config.USE_SENTRY:
    # If an error happens on the same line, ignore it for this amount of time (so we don't burn through our quota)
    SENTRY_MAX_PER_LINE = timedelta(hours=10).total_seconds()
    last_sent = defaultdict(float)

    def sentry_filter(event, hint):
        if 'exc_info' in hint:
            exc_type, exc_value, tb = hint['exc_info']
            tb: TracebackType
            fingerprint = f'{tb.tb_frame.f_lineno}:{tb.tb_frame.f_code.co_filename}'
            if time.time() - last_sent[fingerprint] > SENTRY_MAX_PER_LINE:
                last_sent[fingerprint] = time.time()
                return event
            return None
        return event

    logger.info("Connecting to Sentry")
    import sentry_sdk  # noqa
    sentry_sdk.init(
        dsn=config.SENTRY_DSN,
        before_send=sentry_filter,
        traces_sample_rate=0,
    )


######################
#   FASTAPI SERVER   #
######################

# Set to True when all modules have loaded (used for readiness probe)
READY = False

# The actual server
app = FastAPI(
    title="Razni scraped APIji",
    description="Improviziran API za razne (večinoma slovenske) strani ki nimajo svojih APIjev."
)

# CORS config
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


###############
#   METRICS   #
###############

# Prometheus metrics
instrumentator = Instrumentator().instrument(app)
instrumentator.expose(app, include_in_schema=False)


###################
#   CACHE SETUP   #
###################

# Init cache
if config.USE_CACHE:
    backend = InMemoryBackend()
else:
    backend = DummyCacheBackend()
FastAPICache.init(backend)


#########################
#   LOAD SITE MODULES   #
#########################

SITE_NAMES = OJPP_SITES
if not config.OJPP_ONLY:
    SITE_NAMES += OTHER_SITES

SITES = []
for name in SITE_NAMES:
    try:
        site = importlib.import_module('slo_scrap_api.sites.' + name)
        SITES.append(site)
    except:  # noqa
        import traceback
        traceback.print_exc()

for site in SITES:
    try:
        app.include_router(site.router)
    except:  # noqa
        logger.exception("Error while including site %s" % site.__name__)


############################
#   SITE STARTUP METHODS   #
############################

@app.on_event("startup")
async def on_startup():
    # Some sites have async startup methods, so we can only call them in an async context
    for site in SITES:
        try:
            if hasattr(site, 'setup') and callable(site.setup):
                await site.setup()
        except:  #noqa
            logger.exception("Error while setting up site %s" % site.__name__)

    global READY
    READY = True


########################
#   GLOBAL ENDPOINTS   #
########################

@app.get('/robots.txt')
async def robots():
    return Response(content='User-agent: *\nDisallow: /', media_type='text/plain')

@app.get('/', include_in_schema=False)
async def home():
    return 'OK'

@app.get('/ready', description="Returns 200 OK only when all modules have loaded", include_in_schema=False)
async def ready():
    if READY:
        return Response(status_code=200, content='OK')
    else:
        return Response(status_code=503, content='NOT READY')

@app.get('/debug', include_in_schema=False)
async def debug(request: Request):
    return {
        'headers': dict(request.headers),
        'ip': request.client.host,
        'ok': True
    }


if __name__ == "__main__":
    # Run with uvicorn
    uvicorn.run(app, host="0.0.0.0", port=42069)
