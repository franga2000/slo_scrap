import os

IS_DEV = os.getenv("ENVIRONMENT", default="dev").startswith("dev")

SENTRY_DSN = os.getenv("SENTRY_DSN", default=None)

USE_CACHE = not IS_DEV

USE_SENTRY = SENTRY_DSN and not IS_DEV

OJPP_ONLY = os.getenv("OJPP_ONLY", default="false").lower() in ("true", "True", "1")
