from typing import Dict, List

from fastapi import APIRouter
from fastapi_cache.decorator import cache

from slo_scrap_lib.models import oJPP_Vožnja
from slo_scrap_lib.sites import ap_ljubljana

router = APIRouter(
    prefix="/ap_ljubljana",
    tags=["oJPP"],
)

@router.get(
    "/postaje",
    description="Tabela postaj {id → ime}"
)
@cache(expire=24*60*60)
async def postaje() -> Dict[str, str]:
    return await ap_ljubljana.seznam_postaj()

@router.get(
    "/vozni_red/{vstop_id}/{izstop_id}/{iso_datum}",
    response_model=List[oJPP_Vožnja],
    description="Vožnje od→do na dan datum"
)
@cache(expire=24*60*60)
async def vozni_red(vstop_id: str, izstop_id: str, iso_datum: str, include_details=True):
    return await ap_ljubljana.vozni_red(vstop_id, izstop_id, iso_datum, include_details)
