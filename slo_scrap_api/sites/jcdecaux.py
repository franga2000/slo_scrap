from typing import List

from fastapi import APIRouter
from fastapi_cache.decorator import cache
from geojson_pydantic import Point, FeatureCollection

from slo_scrap_lib.models import RideshareStation, RideshareVehicle
from slo_scrap_lib.sites import jcdecaux

router = APIRouter(
    prefix="/jcdecaux",
    tags=["Izposoja koles (JCDecaux / Europlakat)"],
)

@router.get(
    "/{city}/stations",
    description="GeoJSON FeatureCollection of available bike stands",
    response_model=FeatureCollection[Point, RideshareStation]
)
@cache(expire=60)
async def jcdecaux_stations(city):
    return await jcdecaux.get_stations(city)


@router.get(
    "/{city}/vehicles",
    description="GeoJSON FeatureCollection of available cars",
    response_model=List[RideshareVehicle]
)
@cache(expire=60)
async def vehicles(city, station_id: int):
    return await jcdecaux.get_vehicles(city, station_id)
