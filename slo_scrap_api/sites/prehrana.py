from typing import Dict

from fastapi import APIRouter
from fastapi_cache.decorator import cache

from slo_scrap_lib.sites.margento.prehrana import PrehranaClient

router = APIRouter(
    prefix="/prehrana",
    tags=["Študentska prehrana"],
)


prehrana = PrehranaClient()
async def setup():
    await prehrana.init()


@router.get(
    "/lokali",
    description="Seznam lokalov"
)
@cache(expire=60*60)
async def lokali():
    return await prehrana.get_lokali()


@router.get(
    "/meni/{lokal_id}",
    description="Meni za lokal"
)
@cache(expire=60*60)
async def meni(lokal_id):
    return await prehrana.get_menus(lokal_id)
