from typing import List

from fastapi import APIRouter
from fastapi_cache.decorator import cache

from slo_scrap_lib.models import oJPP_Vožnja
from slo_scrap_lib.sites import prevozi

router = APIRouter(
    prefix="/prevoz",
    tags=["oJPP"],
)

@router.get(
    "/vozni_red/{vstop_id}/{izstop_id}/{iso_datum}",
    response_model=List[oJPP_Vožnja],
    description="Vožnje od→do na dan datum"
)
@cache(expire=24*60*60)
async def vozni_red(vstop_id: str, izstop_id: str, iso_datum: str):
    return await prevozi.vozni_red(vstop_id, izstop_id, iso_datum)
