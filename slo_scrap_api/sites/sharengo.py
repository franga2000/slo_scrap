from fastapi import APIRouter
from fastapi_cache.decorator import cache
from geojson_pydantic import Point, FeatureCollection

from slo_scrap_lib.models import RideshareProperties, RideshareVehicle
from slo_scrap_lib.sites import sharengo

router = APIRouter(
    prefix="/sharengo",
    tags=["Share'N Go"],
)

@router.get(
    "/cars",
    description="GeoJSON FeatureCollection of available cars",
    response_model=FeatureCollection[Point, RideshareVehicle]
)
@cache(expire=60)
async def cars(lat=46.2, lng=15.52, radius=55):
    return await sharengo.available_cars((lat,lng), radius)
