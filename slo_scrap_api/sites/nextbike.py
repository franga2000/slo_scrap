from typing import List

from fastapi import APIRouter
from fastapi_cache.decorator import cache
from geojson_pydantic import Point, FeatureCollection

from slo_scrap_lib.models import RideshareStation, RideshareVehicle
from slo_scrap_lib.sites import nextbike

router = APIRouter(
    prefix="/nextbike",
    tags=["Izposoja koles (Nextbike / Nomago)"],
)

@router.get(
    "/{country}/stations",
    description="GeoJSON FeatureCollection of available bike stands",
    response_model=FeatureCollection[Point, RideshareStation]
)
@cache(expire=60)
async def nextbike_stations(country='SI'):
    return await nextbike.get_stations(country)

@router.get(
    "/vehicles",
    description="List of available vehicles at station",
    response_model=List[RideshareVehicle]
)
@cache(expire=60)
async def vehicles(station_id:str=None):
    return await nextbike.get_vehicles(station_id)
