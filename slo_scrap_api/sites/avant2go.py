import logging
from typing import List

from fastapi import APIRouter
from fastapi_cache.decorator import cache
from geojson_pydantic import Point, FeatureCollection
from starlette.requests import Request

from slo_scrap_lib.models import RideshareStation, RideshareVehicle
from slo_scrap_lib.sites import avant2go

router = APIRouter(
    prefix="/avant2go",
    tags=["Avant2Go"],
)

@router.get(
    "/stations",
    description="GeoJSON FeatureCollection of available car stations",
    response_model=FeatureCollection[Point, RideshareStation]
)
@cache(expire=60)
async def cars():
    return await avant2go.get_stations()


@router.get(
    "/vehicles",
    description="List of available vehicles at station",
    response_model=List[RideshareVehicle]
)
@cache(expire=60)
async def vehicles(request: Request, station_id=None):
    return await avant2go.get_cars(station_id)


@router.get(
    "/vehicles_geojson",
    description="GeoJSON FeatureCollection of available vehicles at station (or all)",
    response_model=FeatureCollection[Point, RideshareVehicle]
)
@cache(expire=60)
async def vehicles(request: Request, station_id=None):
    return await avant2go.get_vehicles_geojson(station_id)
