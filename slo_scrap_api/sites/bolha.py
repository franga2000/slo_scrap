from typing import Optional

from fastapi import APIRouter
from fastapi.encoders import jsonable_encoder
from starlette.responses import JSONResponse

from slo_scrap_lib.sites import bolha
from slo_scrap_lib.sites.bolha import Oglas

router = APIRouter(
    prefix="/bolha",
    tags=["Bolha.com"],
)

@router.get(
    "/oglas",
    response_model=Oglas,
    description="Podatki o enem oglasu."
)
async def bolha_single(url: Optional[str] = None, id: Optional[int] = None):
    if url:
        oglas = await bolha.read_ad(link=url)
    elif id:
        oglas = await bolha.read_ad(ad_id=id)
    else:
        raise Exception('You must provide either URL or ID')
    json = jsonable_encoder(oglas, exclude_none=True)
    return JSONResponse(json)

