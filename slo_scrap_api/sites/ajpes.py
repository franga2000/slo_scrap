from typing import Dict

from fastapi import APIRouter
from fastapi_cache.decorator import cache

from slo_scrap_lib.sites.ajpes.ajpes import AJPES

router = APIRouter(
    prefix="/ajpes",
    tags=["AJPES"],
)

ajpes = AJPES()
async def setup():
    await ajpes.init()


@router.get(
    "/prs/{prs_id}",
    description="Vpogled v poslovni register"
)
@cache(expire=24*60*60)
async def prs_info(prs_id) -> Dict[str, str]:
    return await ajpes.prs_info(prs_id)

