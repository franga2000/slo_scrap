from fastapi import APIRouter
from fastapi_cache.decorator import cache
from slo_scrap_lib.sites import evinjeta

router = APIRouter(
    prefix="/evinjeta",
    tags=["eVinjeta"],
)

@router.get(
    "/validate",
    description="Preveri veljavnost eVinjete za vozilo",
    response_model=dict
)
@cache(expire=60)
async def validate(registrska_oznaka):
    return await evinjeta.validate(registrska_oznaka)

setup = evinjeta.setup
