from datetime import datetime
from typing import Dict, List, Optional

from fastapi import APIRouter, Request
from fastapi_cache.decorator import cache
from starlette.responses import Response

from slo_scrap_api import config_secret
from slo_scrap_lib.models import oJPP_Vožnja, SZ_Lokacija, oJPP_Postaja, SZ_TrainDetails
from slo_scrap_lib.sites import sz

router = APIRouter(
    prefix="/sz",
    tags=["oJPP"],
)


@router.get(
    "/postaje",
    description="Seznam postaj",
)
@cache(expire=24*60*60)
async def seznam_postaj() -> List[oJPP_Postaja]:
    return await sz.seznam_postaj()

@router.get(
    "/lokacije",
    description="Približne lokacije vlakov (potniski.sz.si/info)"
)
@cache(expire=60)
async def potniski_lokacije() -> List[SZ_Lokacija]:
    return await sz.potniski_lokacije()

@router.get(
    "/lokacije_raw",
    description="Približne lokacije vlakov (potniski.sz.si/info) - nepreslikane"
)
@cache(expire=60)
async def potniski_lokacije_raw():
    return await sz.potniski_lokacije_raw()

@router.post(
    '/lokacije_raw',
    include_in_schema=False,
)
async def potniski_lokacije_raw_post(request: Request, key: str):
    if key != config_secret.SZ_POST_KEY:
        return Response(status_code=404, content='Not Found')
    sz.SZ_LOKACIJE_LAST = await request.json()
    sz.SZ_LOKACIJE_TIME = datetime.now()
    return 'OK'

@router.get(
    "/train_details/{train_num}",
    #response_model=SZ_TrainDetails,
)
@cache(expire=60*60)
async def train_details(train_num: str, iso_date: Optional[str] = None) -> SZ_TrainDetails:
    iso_date = iso_date or datetime.now().isoformat()[0:10]
    return await sz.train_details(train_num, iso_date)

@router.get(
    "/vozni_red/{vstop_id}/{izstop_id}/{iso_datum}",
    #response_model=List[oJPP_Vožnja],
    description="Vožnje od→do na dan datum (uporablja potniski.sz.si)"
)
@cache(expire=24*60*60)
async def vozni_red(vstop_id: str, izstop_id: str, iso_datum: str) -> List[oJPP_Vožnja]:
    return await sz.vozni_red(vstop_id, izstop_id, iso_datum)


@router.get(
    "/eshop/postaje",
    description="Tabela postaj {id → ime} (uporablja eshop.sz.si)"
)
@cache(expire=24*60*60)
async def eshop_seznam_postaj() -> Dict[str, str]:
    return await sz.eshop_seznam_postaj()


@router.get(
    "/eshop/vozni_red/{vstop_id}/{izstop_id}/{iso_datum}",
    response_model=List[oJPP_Vožnja],
    description="Vožnje od→do na dan datum (uporablja eshop.sz.si)"
)
@cache(expire=24*60*60)
async def eshop_vozni_red(vstop_id: str, izstop_id: str, iso_datum: str):
    return await sz.eshop_vozni_red(vstop_id, izstop_id, iso_datum)
