from typing import List

from fastapi import APIRouter, Query
from fastapi_cache.decorator import cache
from geojson_pydantic import Point, FeatureCollection

from slo_scrap_lib.models import RideshareStation, RideshareVehicle
from slo_scrap_lib.sites import scbikes

router = APIRouter(
    prefix="/scbikes",
    tags=["Izposoja koles (Smart City Bikes)"],
)

URL_MAP = {
    'soboskibiciklin': 'https://www.soboskibiciklin.si/',
    'gorenjska': 'https://www.gorenjska.bike/',
    'krskolesom': 'https://www.krskolesom.si/',
    'posbikes': 'https://www.posbikes.si/',
    None: None,
}

@router.get(
    "/stations",
    description="GeoJSON FeatureCollection of available bike stations",
    response_model=FeatureCollection[Point, RideshareStation],
)
@cache(expire=60)
async def scb_bikes(system: str = Query(None, enum=list(URL_MAP.keys()))):
    return await scbikes.get_stations(URL_MAP[system])


@router.get(
    "/vehicles",
    description="List of bikes at station",
    response_model=List[RideshareVehicle]
)
@cache(expire=60)
async def scb_vehicles(station_id: int = None):
    return await scbikes.get_vehicles(station_id)
