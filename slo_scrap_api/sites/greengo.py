from fastapi import APIRouter
from fastapi_cache.decorator import cache
from geojson_pydantic import Point, FeatureCollection

from slo_scrap_lib.sites import greengo
from slo_scrap_lib.models import RideshareProperties, RideshareVehicle

router = APIRouter(
    prefix="/greengo",
    tags=["oJPP"],
)


@router.get(
    "/vehicles",
    description="GeoJSON FeatureCollection of available cars",
    response_model=FeatureCollection[Point, RideshareVehicle]
)
@cache(expire=60)
async def cars():
    return await greengo.available_cars()
