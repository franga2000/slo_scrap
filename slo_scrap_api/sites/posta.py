from typing import Dict, List

from fastapi import APIRouter
from fastapi_cache.decorator import cache

import config_secret
from slo_scrap_lib.sites.posta import MojaPosta, PoštnaŠtevilka

router = APIRouter(
    prefix="/posta",
    tags=["Pošta Slovenije"],
)


posta = MojaPosta(config_secret.MOJA_POSTA.username, config_secret.MOJA_POSTA.password)

async def setup():
    await posta.init()

@router.get(
    "/postne_stevilke",
    description="Seznam poštnih številk",
    response_model=List[PoštnaŠtevilka]
)
@cache(expire=60*60*24*7)
async def postne_stevilke():
    return await posta.get_postal_codes()

