from fastapi import APIRouter
from fastapi_cache.decorator import cache

from slo_scrap_lib.sites import wiz

router = APIRouter(
    prefix="/wiz",
    tags=["Zavarovalnica WIZ/G24"],
)


@router.get(
    "/car_data",
    description="Podatki o vozilu"
)
@cache()
async def _car_data(plate: str = '', vin: str = ''):
    assert plate or vin, 'You must provide either plate or vin'
    return await wiz.car_data(plate, vin)
