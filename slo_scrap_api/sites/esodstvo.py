import io
from typing import Optional

from captcha_solver import CaptchaSolver
from fastapi import APIRouter, Query
from starlette.responses import StreamingResponse

from slo_scrap_api import config_secret
from slo_scrap_lib.sites.esodstvo import TipNepremicnine, eVlozisce

router = APIRouter(
    prefix="/esodstvo",
    tags=["e-Sodstvo"],
)

v: eVlozisce = None

def init_if_needed():
    global v
    if v:
        return
    v = eVlozisce(session_file="data/evlozisce_session.pickle", captcha_solver=CaptchaSolver("2captcha", api_key=config_secret.TWOCAPTCHA_API_KEY))
    r1 = v.login(config_secret.EVLOZISCE.EMAIL, config_secret.EVLOZISCE.PASSWORD)

    r2 = v.role_eopravila_reg()

    r3 = v.izberi_ezk()


@router.get(
    "/ezk_izpis",
    responses={
        200: {
            "content": {"application/pdf": {}},
            "description": "PDF result",
        }
    },
    description="PDF izpiz iz e-Zemljiške Knjige. Deluje le med 8:00 in 20:00 (omejitev e-Vložišča). 24h cache"
)
# @cache(expire=24*60*60)
async def ezk_izpis(tip_nepremicnine: str = Query("PARCELA", enum=[tip.name for tip in TipNepremicnine]), katastrska_obcina_št: int = None, številka: str = "", številka_dela: Optional[str] = None) -> StreamingResponse:
    init_if_needed()
    tip = getattr(TipNepremicnine, tip_nepremicnine)
    args = (
        tip,
    )
    kwargs = dict(
        katastrska_občina=katastrska_obcina_št,
    )
    keys = {
        'PARCELA': 'številka_parcele',
        'STAVBA': 'številka_stavbe',
        'DEL_STAVBE': '',
        'STAVBNA_PRAVICA': 'številka_VS',
        'VISEČA_STAVBA': 'številka_VS',
        'NERAZDELJENI_DELI': '',
    }
    kwargs[keys[tip_nepremicnine]] = številka
    if 'DEL' in tip_nepremicnine:
        kwargs['številka_dela_stavbe'] = številka_dela

    resp = v.ezk_izpis(*args, **kwargs)
    response = StreamingResponse(io.BytesIO(resp.content), media_type="application/pdf")
    response.headers["Content-Disposition"] = resp.headers["Content-Disposition"]
    return response


