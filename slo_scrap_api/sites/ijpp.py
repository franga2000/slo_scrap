from typing import Dict, List

from fastapi import APIRouter
from fastapi.encoders import jsonable_encoder
from fastapi_cache.decorator import cache

from slo_scrap_lib.models import oJPP_Vožnja
from slo_scrap_lib.sites import ijpp

router = APIRouter(
    prefix="/ijpp",
    tags=["oJPP"],
)

client: ijpp.IJPP
async def setup():
    global client
    client = ijpp.IJPP()
    await client.init()

@router.get(
    "/postaje",
    description="Tabela postaj {id → ime}"
)
@cache(expire=24*60*60)
async def postaje() -> Dict[str, str]:
    return {p.id: p async for p in client.get_stations()}

@router.get(
    "/vozni_red/{vstop_id}/{izstop_id}/{iso_datum}",
    response_model=List[oJPP_Vožnja],
    description="Vožnje od→do na dan datum"
)
@cache(expire=24*60*60)
async def vozni_red(vstop_id: str, izstop_id: str, iso_datum: str):
    return await client.vozni_red(vstop_id, izstop_id, iso_datum)


def _json_unwrap(dic: dict):
    if '__values__' in dic:
        return _json_unwrap(dic['__values__'])
    else:
        for key in dic.keys():
            if type(dic[key]) == dict:
                dic[key] = _json_unwrap(dic[key])
            elif type(dic[key]) == list:
                dic[key] = [_json_unwrap(d) if type(d) in (dict, list) else d for d in dic[key] ]
        return dic

@router.get(
    "/lokacije_raw",
    description="Lokacije avtobusov"
)
@cache(expire=10)
async def vozni_red():
    return [_json_unwrap(d) for d in jsonable_encoder(await client.get_vehicle_locations_raw())]


@router.get(
    "/lokacije_raw",
    description="Lokacije avtobusov"
)
@cache(expire=10)
async def vozni_red():
    return await client.get_vehicle_locations()
