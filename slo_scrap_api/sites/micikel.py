from fastapi import APIRouter
from fastapi_cache.decorator import cache
from geojson_pydantic import Point, FeatureCollection

from slo_scrap_lib.models import RideshareStation
from slo_scrap_lib.sites import micikel

router = APIRouter(
    prefix="/micikel",
    tags=["Izposoja koles (micikel)"],
)

@router.get(
    "/stations",
    description="GeoJSON FeatureCollection of available bike stands",
    response_model=FeatureCollection[Point, RideshareStation]
)
@cache(expire=60)
async def micikel_stations():
    return await micikel.get_stations()
