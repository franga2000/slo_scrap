import aiohttp
from fastapi import APIRouter
from fastapi_cache.decorator import cache

from slo_scrap_api import config_secret

router = APIRouter(
    prefix="/3rd_party",
    tags=["3rd party APIs (caching proxy)"],
)


@router.get(
    "/google_places_autocomplete",
    description="Places Autocomplete API",
)
@cache(expire=60)
async def google_places_autocomplete(input, components='country:si', **kwargs):
    url = 'https://maps.googleapis.com/maps/api/place/autocomplete/json'
    params = {
        'key': config_secret.GOOGLE_PLACES_KEY,
        'components': components,
        'input': input,
    } | kwargs
    with aiohttp.request('GET', url, params=params) as resp:
        return await resp.text()
