from datetime import datetime
from typing import Dict, List

from fastapi import APIRouter
from fastapi_cache.decorator import cache

from slo_scrap_lib.sites.margento.marprom import MarpromClient
from slo_scrap_lib.sites import marprom

router = APIRouter(
    prefix="/marprom",
    tags=["Marprom"],
)


@router.get(
    "/postaje",
    description="Tabela postaj {id → ime,lokacija...}"
)
@cache(expire=24 * 60 * 60)
async def postaje() -> Dict[str, str]:
    return await marprom.seznam_postaj()


@router.get(
    "/odhodi/{vstop_id}",
    response_model=Dict[str, Dict[str, List[datetime]]],
    responses={
        200: {
            "description": "Odhodi s postaje {vstop_id} na datum {iso_datum}",
            "content": {
                "application/json": {
                    "example": {
                        "št_linije_1": {
                            "Neka smer": ["2021-11-25T04:59:00", "2021-11-25T05:39:00"],
                            "Neka druga smer": ["2021-11-25T13:59:00", "2021-11-25T14:24:00"],
                        },
                        "št_linije_2": {
                            "Neka smer": ["2021-11-25T04:59:00", "2021-11-25T05:39:00"],
                        },
                    }
                }
            },
        },
    },
    description="Odhodi s postaje {vstop_id} na datum {iso_datum}"
)
@cache(expire=24 * 60 * 60)
async def vozni_red(vstop_id: str, iso_datum: str):
    return await marprom.odhodi(vstop_id, iso_datum)


api = None
async def get_api():
    global api
    if api:
        return api
    api = MarpromClient()
    await api.init()
    return api

@router.get(
    '/get_card_products',
    description='List products on card'
)
@cache(expire=60)
async def get_card_products(CUID: str) -> list[dict]:  # noqa: uppercase
    mp = await get_api()
    resp = await mp.get_card_products(CUID)
    assert resp.get('ResultCode', 0) == 0, f"Error {resp['ResultCode']}: {resp['ResultDescription']}"
    return resp['CardProducts']

