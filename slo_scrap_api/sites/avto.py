from typing import Optional

from fastapi import APIRouter
from fastapi_cache.decorator import cache

from slo_scrap_lib.sites import avto
from slo_scrap_lib.sites.avto import Avto, string_from_to

router = APIRouter(
    prefix="/avto_net",
    tags=["Avto.net"],
)

@router.get(
    "/avto/{avto_id}",
    response_model=Avto,
    description="Podatki o enem oglasu"
)
@cache(expire=60*60)
async def avto_single(avto_id: int) -> Avto:
    a = avto.parse_single(id=avto_id)
    if a.eurotax and a.eurotax.href:
        a.eurotax.href = router.url_path_for("eurotax") + string_from_to(a.eurotax.href, "?")
    return a

@router.get(
    "/eurotax",
    description="Eurotax podatki"
)
@cache(expire=60*60)
async def eurotax(IDE: int, letnik: int, znamka: Optional[str] = None, model: Optional[str] = None) -> dict:
    params = dict(ide=IDE, letnik=letnik)
    if znamka:
        params["znamka"] = znamka
    if model:
        params["model"] = model
    return avto.eurotax(**params)
