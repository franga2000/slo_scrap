from fastapi import APIRouter
from fastapi_cache.decorator import cache
from geojson_pydantic import Point, FeatureCollection

from slo_scrap_lib.models import RideshareVehicle
from slo_scrap_lib.sites import zapeljime

router = APIRouter(
    prefix="/zapeljime",
    tags=["Izposoja koles (Zapelji.me)"],
)

@router.get(
    "/vehicles",
    description="GeoJSON FeatureCollection of available bikes",
    response_model=FeatureCollection[Point, RideshareVehicle]
)
@cache(expire=60)
async def zapeljime_stations():
    return await zapeljime.get_stations()
