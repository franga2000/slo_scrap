from typing import List, Optional, Any

from fastapi import APIRouter
from fastapi_cache.decorator import cache
from pydantic import BaseModel

from slo_scrap_lib.sites.margento.urbanaplus import UrbanaPOIType, UrbanaPlusClient
from slo_scrap_lib.models import oJPP_Kiosk

router = APIRouter(
    prefix="/urbanaplus",
    tags=["oJPP"],
)

urbana = UrbanaPlusClient()


@router.get(
    "/kiosks",
    response_model=List[oJPP_Kiosk],
)
@cache(expire=24*60*60)
async def kiosks():
    return await urbana.get_kiosks()


class POI(BaseModel):
    id: int
    bikeStatus: Any
    direction: Optional[int]
    latitude: float
    longitude: float
    name: str
    nameNumber: Optional[str]
    otherId: Optional[int]
    routes: str
    status: int
    supertype: int
    type: UrbanaPOIType
    zone: Optional[int]


@router.get(
    "/pois",
    response_model=List[POI],
)
@cache(expire=24*60*60)
async def pois():
    return await urbana.get_pois()
