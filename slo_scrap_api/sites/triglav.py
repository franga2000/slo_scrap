from fastapi import APIRouter, HTTPException
from fastapi_cache.decorator import cache

from slo_scrap_lib.sites import triglav

router = APIRouter(
    prefix="/cars",
    tags=["Car data"],
)


@router.get(
    "/car_data",
    description="Podatki o vozilu"
)
@cache()
async def _car_data(plate: str = '', vin: str = ''):
    if not (plate or vin):
        raise HTTPException(status_code=400, detail="Either plate or vin must be provided")
    return await triglav.car_data(plate, vin)
