from typing import Dict, List

from fastapi import APIRouter, HTTPException
from fastapi_cache.decorator import cache

from slo_scrap_lib.sites import hi3
from slo_scrap_lib.sites.hi3 import EurotaxResult, Zavarovanja, BackendException

router = APIRouter(
    prefix="/hi3",
    tags=["Hi3 (Zavarovalnica Sava)"],
)

@router.get(
    "/eurotax_search",
    description="Išče vozila po bazi Eurotax",
    response_model=List[EurotaxResult]
)
@cache(expire=24*60*60)
async def eurotax_search(letnik: int, naziv: str) -> List[EurotaxResult]:
    return hi3.eurotax_search(letnik, naziv)

@router.get(
    "/zavarovanja/",
    description="Seznam zavarovanj na voljo in njihovih cen",
    response_model=Zavarovanja,
)
@cache(expire=24*60*60)
async def zavarovanja(eurotax, letnik, naziv):
    try:
        return hi3.zavarovanja(eurotax, letnik, naziv)
    except BackendException as e:
        raise HTTPException(
            status_code=400,
            detail=e.data,
        )
