# 

## Project structure

 - `slo_scrap_api/` - FastAPI project
   - `sites/` - request routers for all covered services
   - `main.py` - server entrypoint - new sites need to be included here
   - `secrets.py` - 
 - `slo_scrap_lib/` - scraping modules (usable independently of the API)
   - `sites/` - scrapers for all covered services
   - `tests/` - unit tests for shared functions
   - `models.py` - model definitions that are used by multiple sites (Pydantic or dataclasses)
