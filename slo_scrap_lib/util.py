from datetime import datetime
from typing import Iterable
import re


def flatten(lst_lst: Iterable[Iterable]) -> Iterable:
    for lst in lst_lst:
        for el in lst:
            yield lst


def pairwise(iterable: Iterable):
    prev = None
    for itm in iterable:
        if prev is not None:
            yield prev, itm
            prev = None
        else:
            prev = itm


def hour_on_date(date: datetime, time: str):
    hr, min = time.split(":")
    return date.replace(hour=int(hr), minute=int(min))


TIME_RE = re.compile(r'(\d{1,2}):(\d{1,2})')
