import dataclasses
from datetime import datetime, date
from enum import Enum
from typing import List, Optional, Tuple

from pydantic import BaseModel


class RideshareProvider(str, Enum):
    AVANT2GO = 'avant2go'
    SHARENGO = 'sharengo'
    GREENGO = 'greengo'
    BICIKELJ = 'bicikelj'
    EUROPLAKAT = 'europlakat'
    NOMAGO_BIKES = 'nomago_bikes'
    SMART_CITY_BIKES = 'smart_city_bikes'
    MICIKEL = 'micikel'
    ZAPELJI_ME = 'zapelji_me'


class RidesharePointType(str, Enum):
    CAR_FLOATING = 'car_floating'
    SCOOTER_FLOATING = 'scooter_floating'
    CAR_STATION = 'car_station'
    BIKE_STATION = 'bike_station'
    BIKE_FLOATING = 'bike_floating'


class Vrsta(str, Enum):
    BUS = 'bus'
    VLAK = 'train'
    KOLO = 'bike'
    AVTO = 'car'


class RideshareProperties(BaseModel):
    # Name of the sharing system, which might be only one of many operated by the same company
    system: str = None
    # A company, providing the sharing service. Different companies will have very different terms
    provider: RideshareProvider = None
    type: RidesharePointType = None
    id: str
    title: str = None
    image_url: str = None


class RideshareVehicle(RideshareProperties):
    license_plate: str = None
    vin: str = None
    charge_level: float = None
    is_charging: bool = None
    model: str = None
    people_capacity: int = None
    range_estimate: int = None
    price_per_km: float = None
    price_per_min: float = None
    price_minimum: float = None
    price_start: float = None
    deeplink: str = None
    number_in_station: int = None
    rating: float = None
    ratings: int = None
    station_id: Optional[str] = None


class RideshareStation(RideshareProperties):
    capacity: int
    vehicles_available: int
    capacity_free: int
    address: str = None


class oJPP_Postaja(BaseModel):
    id: str = None
    naziv: str = None
    lat: float = None
    lng: float = None


class oJPP_Vožnja(BaseModel):
    """Predstavlja eno vožnjo na javnem potniškem prometu"""
    odhod: datetime = None
    prihod: datetime = None
    naziv: str = None
    prevoznik: str = None
    razdalja: float = None
    opozorila: List[str] = []
    more_detail: Optional[str] = None
    peron: str = None
    id: str = None
    cena: float = None

    def __hash__(self):
        return hash((self.odhod, self.prihod, self.naziv, self.prevoznik, self.razdalja))

    class Config:
        #frozen = True
        allow_mutation = True


class oJPP_LokacijaVozila(BaseModel):
    """Predstavlja eno vozilo z geografsko lokacijo"""
    vozilo_id: str
    lat: float
    lng: float
    linija_id: str = None
    prevoznik: str = None
    vrsta: Vrsta = None


class SZ_Lokacija(BaseModel):
    st_vlaka: str
    rang_vlaka: str
    lat: float
    lng: float
    relacija: str
    naslednja_postaja: str
    postaja_eta_min: int
    zamuda_min: int
    bus: bool = False
    raw_odhod: str
    raw_st: str


class SZ_TrainDetailsStop(BaseModel):
    name: str
    id: str = None
    arrival: str = None
    departure: str = None

class SZ_TrainDetails(BaseModel):
    #train: str
    date: date
    timetable_description: str = None
    validity_description: str = None
    stops: List[SZ_TrainDetailsStop] = []

class Dir(Enum):
    UNKNOWN = 0
    TO_CENTER = 2
    FROM_CENTER = 1

@dataclasses.dataclass(unsafe_hash=True)
class Stop:
    id: str
    name: str
    ids: dict = None
    lat: float = None
    lng: float = None
    lines: List["LineInfo"] = None
    dir: int = None


@dataclasses.dataclass(unsafe_hash=True)
class LineInfo:
    id: int = None
    ids: dict = None
    name: str = None
    heading: str = None
    dir: int = None
    stops: List[Stop] = dataclasses.field(hash=False, default=None)
    paths: List[List[Tuple[float, float]]] = dataclasses.field(hash=False, default=None)


@dataclasses.dataclass(unsafe_hash=True)
class TransitInfo:
    lines: List[LineInfo]
    stops: List[Stop]


@dataclasses.dataclass(unsafe_hash=True)
class oJPP_Kiosk:
    id: str
    name: str
    lat: float
    lng: float
