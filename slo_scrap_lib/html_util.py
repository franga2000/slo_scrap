import re
from typing import List, Dict, Tuple

from bs4 import Tag
from lxml.html import HtmlElement

RE_WHITESPACE = re.compile(r"\s+")


def textify(el: Tag) -> str:
    if type(el) == Tag:
        return RE_WHITESPACE.sub(" ", el.text).strip()
    else:
        return RE_WHITESPACE.sub(" ", el.text_content()).strip()


def html_list_to_list(html_list) -> List[str]:
    lst = []
    for el in html_list.find_all("li"):
        lst.append(textify(el))
    return lst


def clean_table_key(cell) -> str:
    return cell.rstrip(":").strip()


def table_to_matrix(table: Tag) -> List[List[Tag]]:
    matrix = []
    for tr in table.select("tr"):
        row = []
        for td in tr.select("td,th"):
            row.append(td)
        matrix.append(row)
    return matrix


def table_to_dict(table: HtmlElement = None, matrix: List[List[str]] = None) -> Dict[str, str]:
    """Read a key-value style table to a dict"""
    if matrix is None:
        matrix = table_to_matrix(table)
    dic = {}
    for row in matrix:
        dic[clean_table_key(row[0])] = row[1]
    return dic


def table_to_dict_list(table: Tag) -> List[Dict[str, str]]:
    """Read a table with headers into a list of rows"""
    trs = table.select("tr")
    ths = trs[0].select("td,th")
    headers = [textify(th) for th in ths]
    lst = []
    for row in trs[1:]:
        tds = row.select("td")
        values = [textify(td) for td in tds]
        lst.append({headers[i]: v for i, v in enumerate(values)})
    return lst


def select_to_tuples(select: Tag) -> Tuple[str, str]:
    for el in select.select("option"):
        el: Tag
        yield el.attrs["value"], el.text


def select_to_dict(select: Tag) -> Dict[str, str]:
    dic = {}
    for el in select.select("option"):
        el: Tag
        dic[el.attrs["value"]] = el.text
    return dic


def extract_form_fields(soup, select_contents=False) -> Dict[str, str]:
    "Turn a BeautifulSoup form in to a dict of fields and default values"
    fields = {}
    for input in soup.findAll('input'):
        if not 'type' in input:
            continue
        # ignore submit/image with no name attribute
        if input['type'] in ('submit', 'image') and not 'name' in input:
            continue

        # single element nome/value fields
        if input['type'] in ('text', 'hidden', 'password', 'submit', 'image'):
            value = ''
            if 'value' in input:
                value = input['value']
            fields[input['name']] = value
            continue

        # checkboxes and radios
        if input['type'] in ('checkbox', 'radio'):
            value = ''
            if input.has_attr("checked"):
                if input.has_attr('value'):
                    value = input['value']
                else:
                    value = 'on'
            if 'name' in input and value:
                fields[input['name']] = value

            if not 'name' in input:
                fields[input['name']] = value

            continue

        assert False, 'input type %s not supported' % input['type']

    # textareas
    for textarea in soup.findAll('textarea'):
        fields[textarea['name']] = textarea.string or ''

    # select fields
    for select in soup.findAll('select'):
        value = ''
        options = select.findAll('option')
        is_multiple = select.has_key('multiple')
        selected_options = [
            option for option in options
            if option.has_key('selected')
        ]

        # If no select options, go with the first one
        if not selected_options and options:
            selected_options = [options[0]]

        if not is_multiple:
            assert (len(selected_options) < 2)
            if len(selected_options) == 1:
                if select_contents:
                    value = selected_options[0].text.strip()
                else:
                    value = selected_options[0]['value']
        else:
            if select_contents:
                value = [option.text.strip() for option in selected_options]
            else:
                value = [option['value'] for option in selected_options]

        fields[select['name']] = value

    return fields
