import json
from base64 import b64encode

import aiohttp

from slo_scrap_api import config_secret


class BoltClient:
    _headers = {}

    def __init__(self):
        self.phone_num = config_secret.BOLT_PHONE_NUM

    async def init(self):
        self._headers = {
            'Authorization': b64encode(self.phone_num + ':' + self.phone_num)
        }

    async def authenticated_request(self):
        params = {
            'lat': 46.0511,
            'lng': 14.5051,
            'version': 'CI.24.0',
            'deviceId': self.phone_num,
            'deviceType': 1,
            'device_name': 1,
            'device_os_version': 1,
            'language': 'sl',
        }
        async with aiohttp.request('POST', 'https://rental-search.bolt.eu/categoriesOverview', params=params) as resp:
            resp = await resp.json()

        features = []
        for vehicle in (c['vehicles'] for c in resp['data']['categories']):
            feat = {
                "id": vehicle['id'],
                "geometry": {
                    "type": "Point",
                    "coordinates": [place.attrib['lng'], place.attrib['lat']]
                },
                "properties": {
                    "title": place.attrib['name'],
                    "id": place.attrib['uid'],
                    "type": RidesharePointType.BIKE_STATION,
                    "system": RideshareSystem.NOMAGO_BIKES,
                    "capacity": capacity,
                    "capacity_free": place.attrib['free_racks'],
                }
            }
            features.append(feat)
        return {
            "type": "FeatureCollection",
            "features": features
        }


if __name__ == '__main__':
    bolt = BoltClient()