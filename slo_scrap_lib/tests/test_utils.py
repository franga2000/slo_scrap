import json
from os import PathLike

from bs4 import BeautifulSoup


def read_to_str(filename: PathLike) -> str:
    with open(filename) as f:
        return f.read()


def json_from_file(filename):
    with open(filename) as f:
        return json.load(f)

def soup_from_file(filename):
    with open(filename) as f:
        return BeautifulSoup(f.read())
