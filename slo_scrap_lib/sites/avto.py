from collections import defaultdict
from enum import Enum
from typing import Dict, List, Optional
from urllib.parse import urlparse, parse_qs

from bs4 import BeautifulSoup
from fastapi import HTTPException
from pydantic import BaseModel
from requests import Request

from slo_scrap_lib.fetcher import AutoProxyFetcher
from slo_scrap_lib.html_util import table_to_matrix, textify, html_list_to_list
from slo_scrap_lib.util import pairwise


class Fetcher(AutoProxyFetcher):
    DEFAULT_HEADERS = AutoProxyFetcher.DEFAULT_HEADERS | {
        "Accept": "*/*",
    }

FETCHER = Fetcher()


def table_cells_to_dict(cells, clean_key=lambda s: textify(s).strip(":"), clean_value=textify):
    dic = {}
    for key_cell, val_cell in pairwise(cells):
        key = clean_key(key_cell)
        if not key: continue
        dic[key] = clean_value(val_cell)
    return dic

class StanjeOglasa(int, Enum):
    NA_VOLJO = 1
    PRODANO = 2
    ODSTRANJENO = 3

class Eurotax(BaseModel):
    id: int
    href: Optional[str]

class Avto(BaseModel):
    naslov: str
    stanje: StanjeOglasa
    osnovni_podatki: Dict[str, str]
    zmogljivost_ev: Dict[str, str]
    ostali_podatki: Dict[str, List[str]]
    poraba: Optional[Dict[str, str]]
    cena: str = None
    opombe: str = None
    imgs: List[str] = []
    eurotax: Optional[Eurotax]
    model: Optional[str]
    znamka: Optional[str]


def clean_text_content(string: str) -> str:
    string = string.replace("\r\n", "")
    string = string.replace("\t", " ")
    for i in range(8):
        string = string.replace("  ", " ")
    return string


def parse_single(url: str=None, id: int=None):
    if url is None:
        url = f"https://www.avto.net/Ads/details.asp?id={id}"

    resp = FETCHER.ensure_request(Request("GET", url))
    resp.raise_for_status()

    resp_content = resp.content.decode("windows-1250")
    stanje = StanjeOglasa.NA_VOLJO

    if "unvalid" in resp.url or "Oglas je že odstranjen iz ponudbe" in resp_content:
        raise HTTPException(status_code=404, detail="Oglas je že odstranjen iz ponudbe")

    if "PRODANO. Oglas ni več aktualen" in resp_content:
        stanje = StanjeOglasa.PRODANO

    content = string_from_to(resp_content, "-- TITLE --", "--  MODAL AD REPORT   --")

    html = BeautifulSoup(content)

    avto = Avto(
        naslov=html.select(".col-12 h3")[0].text,
        stanje=stanje,
        osnovni_podatki={},
        ostali_podatki={},
        zmogljivost_ev={},
    )

    for table in html.find_all("table"):
        cells = table.find_all(["td", "th"])
        hdr = textify(cells[0])
        cells = cells[1:]
        if hdr == "Osnovni podatki":
            avto.osnovni_podatki = table_cells_to_dict(cells)
            avto.osnovni_podatki["Motor"] = clean_text_content(avto.osnovni_podatki["Motor"])
            address_script = [s for s in reversed(html.find_all("script")) if s.contents and "DealerAddress" in s.contents[0]]
            if address_script:
                avto.osnovni_podatki["Kraj ogleda"] = string_from_to(str(address_script[0]), "'", "'")[1:-1]
        elif hdr == "Oprema in ostali podatki o ponudbi":
            avto.ostali_podatki = table_cells_to_dict(cells, clean_value=html_list_to_list)
        elif hdr == "Poraba goriva in emisije":
            avto.poraba = table_cells_to_dict(cells)
        elif hdr == "Zmogljivosti EV":
            avto.zmogljivost_ev = table_cells_to_dict(cells)
        elif hdr not in ('Zgodovina vozila',):
            pass

    podobni_txt = html.find(text="iste znamke in modela")
    if podobni_txt:
        params = parse_qs(urlparse(podobni_txt.parent.parent.attrs["href"]).query)
        avto.model = params["model"][0]
        avto.znamka = params["znamka"][0]

    opombe_content = string_from_to(content, "-- OPOMBE --", "INDIVIDUAL IFRAME")
    opombe_html = BeautifulSoup(opombe_content).find("div")
    if opombe_html:
        avto.opombe = opombe_html.get_text("\n").strip()

    cena_content = string_from_to(content, "-- AKCIJSKA CENA TXT--", "-- FINANCING --")
    cena_html = BeautifulSoup(cena_content).find("div")
    if cena_html:
        avto.cena = textify(cena_html)

    #avto.imgs = [img.src.replace("_small", "") for img in html.select("#lightgallery img")]
    avto.imgs = [html.select(".img-fluid.w-100")[0].attrs["src"].replace("_small", "")]

    eurotax_a = html.select_one("a[href*=EUROTAX]")
    if eurotax_a:
        eurotax_href = eurotax_a.attrs["href"]
        params = parse_qs(urlparse(eurotax_href).query)
        eurotax = Eurotax(
            id=int(params["IDE"][0]),
            href=eurotax_href,
        )
        avto.eurotax = eurotax

    return avto


# ide, letnik, znamka=None, model=None
def eurotax(**params):
    url = f"https://www.avto.net/_EUROTAX/mycar.asp"
    req = Request("GET", url, params=params)
    resp = FETCHER.ensure_request(req)
    resp_content = resp.content.decode("windows-1250")

    content = string_from_to(resp_content, " START VSEBINA ", " END VSEBINA ")

    html = BeautifulSoup(content)
    table1 = html.select_one("table")
    naziv = textify(table1.select_one(".naslovB"))

    table2 = table1.select_one("table")
    matrix = table_to_matrix(table2)

    header = None
    data = defaultdict(dict)
    for row in matrix:
        if len(row) == 1:
            header = textify(row[0]).strip(":")
        else:
            k, v = [textify(td).strip(":") for td in row]
            data[header][k] = v

    return dict(
        naziv=naziv,
        **data
    )



def string_from_to(content, match1=None, match2=None):
    start = 0
    end = len(content)
    if match1:
        start = content.find(match1)
    if match2:
        end = content.find(match2, start+1)
    return content[start:end+1]


if __name__ == '__main__':
    #et = eurotax(IDE=345137, letnik=2011)
    #print(et)
    a = parse_single(id=18136202)
    print(a)

