import asyncio
import logging
from typing import Optional

import aiohttp
from geojson_pydantic import FeatureCollection, Point, Feature

from slo_scrap_lib.models import RideshareProvider, RidesharePointType, RideshareVehicle

FLEET_MAP = {
    544: 'GreenGo',
    603: 'Logatec',
    1117: 'Kranj',
    1238: 'Trzin',
}

IMAGE_MAP = {
    '1088': 'https://derp1.blob.core.windows.net/public/greengo_twingo.png',  # Twingo
    '1104': 'https://derp1.blob.core.windows.net/public/greengo_zoe.png',  # ZOE
    '1523': 'https://derp1.blob.core.windows.net/public/greengo_zoe.png',  # ZOE
     '765': 'https://derp1.blob.core.windows.net/public/greengo_zoe.png',  # ZOE
    '1935': 'https://derp1.blob.core.windows.net/public/greengo_zoe.png',  # ZOE (Kranj, Trzin)
    '2206': 'https://derp1.blob.core.windows.net/public/greengo_zoe.png',  # ZOE 2019 (Trzin)  # TODO: change picture to 2019 ZOE
    '1816': 'https://derp1.blob.core.windows.net/public/cupra_born.png',  # Cupra Born (Kranj)
    '1756': 'https://derp1.blob.core.windows.net/public/id3.png',  # iD3 (Kranj)
    '1470': 'https://derp1.blob.core.windows.net/public/greengo_zoe.png'  # ZOE (Logatec)
}


def get_img(car) -> Optional[str]:
    if car['vehicle_type_id'] in IMAGE_MAP:
        return IMAGE_MAP[car['vehicle_type_id']]
    logging.error('No image for GreenGo vehicle', car)
    return None


async def available_cars():
    features = []

    for system_id, system_name in FLEET_MAP.items():
        url = "https://greengo.rideatom.com/gbfs/v2_2/en/free_bike_status?id=" + str(system_id)
        async with aiohttp.request("GET", url) as resp:
            free_vehicles = await resp.json()
        url = 'https://greengo.rideatom.com/gbfs/v2_2/en/system_pricing_plans?id=' + str(system_id)
        async with aiohttp.request("GET", url) as resp:
            data = await resp.json()
            pricing_map = {t['plan_id']: t for t in data['data']['plans']}

        for _car in free_vehicles['data']['bikes']:
            if _car['is_reserved'] or _car['is_disabled']:
                continue

            point = Point(coordinates=(_car['lon'], _car['lat']))
            pricing = pricing_map[_car['pricing_plan_id']]
            plate = _car['rental_uris']['android'].rsplit('/', maxsplit=1)[-1]

            feat = Feature(
                id=_car['bike_id'],
                geometry=point,
                properties=RideshareVehicle(
                    id=_car['bike_id'],
                    title=plate,
                    license_plate=plate,
                    range_estimate=int(_car['current_range_meters'] / 1000),
                    price_start=pricing['price'],
                    price_per_min=pricing['per_min_pricing'][0]['rate'],
                    price_per_km=pricing['per_km_pricing'][0]['rate'],
                    model=pricing['name'],
                    deeplink=_car['rental_uris']['android'],
                    image_url=get_img(_car),
                    type=RidesharePointType.CAR_FLOATING,
                    provider=RideshareProvider.GREENGO,
                    system=RideshareProvider.GREENGO,
                ).dict()
            )
            features.append(feat)

    return FeatureCollection(features=features)


if __name__ == '__main__':
    ft = asyncio.get_event_loop().run_until_complete(available_cars())
    print(ft)
