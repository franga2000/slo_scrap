import asyncio

import aiohttp
from cache import AsyncTTL
from geojson_pydantic import FeatureCollection, Point, Feature

from slo_scrap_lib.models import RideshareProperties, RideshareProvider, RidesharePointType, RideshareVehicle


@AsyncTTL(time_to_live=60*60*5)
async def get_car_types():
    url = 'https://platform.api.gourban.services/v1/sharengo/front/vehicles/categories'
    async with aiohttp.request("GET", url) as resp:
        data = await resp.json()
    return {o['id']: o for o in data}


async def available_cars(center_latlng=(46.28,15.52), radius=55):
    url = "https://sharengo.core.gourban.services/front/vehicles"
    params = {
        "lat": center_latlng[0],
        "lng": center_latlng[1],
        "rad": radius
    }

    async with aiohttp.request("GET", url, params=params) as resp:
        data = await resp.json()

    types = await get_car_types()

    features = []
    for _car in data:
        point = Point(**_car['position'])
        del _car['position']
        type = types.get(_car['categoryId'])
        feat = Feature(
            id=_car['id'],
            geometry=point,
            properties=RideshareVehicle(
                title=_car['name'],
                id=_car['id'],
                license_plate=_car['licensePlate'],
                charge_level=_car['stateOfCharge'],
                is_charging=_car['chargingState'] != 'UNPLUGGED',
                image_url=type['appProperties']['image'],
                model=type['name'],
                deeplink='https://go.gourban.co/sharengo/map?vehicleCode=' + _car['code'],
                type=RidesharePointType.CAR_FLOATING,
                provider=RideshareProvider.SHARENGO,
                system=RideshareProvider.SHARENGO,
            ).dict()
        )
        features.append(feat)

    return FeatureCollection(features=features)


if __name__ == '__main__':
    ft = asyncio.get_event_loop().run_until_complete(available_cars())
    print(ft)
