import asyncio
import json

import aiohttp
from cache import AsyncTTL
from geojson_pydantic import FeatureCollection, Point, Feature

from slo_scrap_lib.fetcher import BaseFetcher
from slo_scrap_lib.models import RideshareProvider, RidesharePointType, RideshareStation, RideshareVehicle

operators = {}


async def ids_by_extent(extent):
    if not extent:
        return operators.keys()

async def get_stations(operator_id):
    url = 'https://mobile.scbikes.com/api/stations/' + str(operator_id) + '/public-list-with-bikes?format=json'
    async with aiohttp.request("GET", url) as resp:
        return await resp.json()

get_stations_cached = AsyncTTL(time_to_live=30)(get_stations)


station_operator_map = {}


async def get_stations(operator_id=None, extent=None):
    global operators
    if not operators:
        url = 'https://mobile.scbikes.com/api/operators'
        async with aiohttp.request("GET", url) as resp:
            operators = {o['id']: o for o in await resp.json() if o['id'] < 1000}

    if operator_id:
        ids = [operator_id]
    else:
        ids = await ids_by_extent(extent)

    features = []

    for operator_id in ids:
        data = await get_stations_cached(operator_id)

        for station in data:
            station_operator_map[station['id']] = operator_id
            system_id = operators[operator_id]['domain'].split('//', maxsplit=1)[1].split('.', maxsplit=1)[0]
            if not station['latitude']: continue
            point = Point(coordinates=(station['longitude'], station['latitude']))
            feat = Feature(
                id=station['id'],
                geometry=point,
                properties=RideshareStation(
                    title=station['name'],
                    id=station['id'],
                    type=RidesharePointType.BIKE_STATION,
                    provider=RideshareProvider.SMART_CITY_BIKES,
                    system=system_id,
                    capacity=station['numberOfLocks'],
                    capacity_free=station['numberOfFreeLocks'],
                    vehicles_available=station['numberOfBikes'],
                ).dict()
            )
            features.append(feat)

    return FeatureCollection(features=features)


async def get_vehicles(station_id):
    station_id = int(station_id)
    if not station_operator_map:
        await get_stations()
    operator_id = station_operator_map[station_id]
    data = await get_stations_cached(operator_id)

    for station in data:
        if station['id'] == station_id:
            break
    else:
        raise Exception('Station not found')

    cars = []
    for item in station['bikes']:
        car = RideshareVehicle(
            id=item['id'],
            model=",".join(item['categories']),
            title=item['name'],
        )
        cars.append(car)

    return cars


if __name__ == '__main__':
    async def main():
        ft = await get_vehicles(127)
        print(ft)

    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
