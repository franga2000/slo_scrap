from datetime import datetime
from typing import List
from warnings import warn

import aiohttp
import lxml.html as lhtml
import requests
from pydantic import BaseModel
from requests import Request

from slo_scrap_lib.fetcher import AutoProxyFetcher
from slo_scrap_lib.html_util import textify
from slo_scrap_lib.util import pairwise


class Fetcher(AutoProxyFetcher):
    pass

FETCHER = Fetcher()

class Oglas(BaseModel):
    raw_info: dict
    price: float = None
    cena_na_m2: bool = None
    title: str = None
    img_urls: List[str] = None
    datum_objave: datetime = None
    description: str = None


def fetch_pages(path, params: dict = {}, start_page=1, base_url="http://www.bolha.com"):
    # TODO: stara
    if "location" in params:
        for l in params["location"]:
            assert l.endswith("/")

    # Properly format lists
    for k, v in params.items():
        if type(v) == list:
            params[k] = "%7C%7C".join(v)

    url = base_url + path
    print("Starting with URL " + url)
    print("With params: " + str(params))

    params["page"] = start_page
    while True:
        resp = requests.get(url, params=params, )
        if resp.status_code != 200:
            warn("Error received from page", params["page"])
        html = resp.text
        dom = lhtml.fromstring(html)
        dom.make_links_absolute(resp.url)
        print("Fetched page", params["page"])
        yield dom

        if len(dom.cssselect(".Pagination-item--next")) == 0:
            break

        # if "blur" in list(dom.cssselect(".pager .forward")[0].classes):
        # 	break
        params["page"] += 1
    raise StopIteration


def find_ad_links(pages):
    for page in pages:
        print("  Processing page", page.cssselect(".Pagination-items strong")[0].text_content().replace("Stran ", ""))
        #		for ad in page.cssselect("#list .ad:not([style]):not(.featured)"):
        for ad in page.cssselect(".EntityList-item--Regular"):
            #			link = ad.cssselect(".content h3 a")[0].get("href")
            link = ad.cssselect(".link")[0].get("href")
            yield link


def parse_description(ad):
    # Find all section titles
    opis = ad.cssselect("#box-oglas-levo")[0]
    sections = []
    for el in opis:
        if el.tag in ("strong", "b"):
            section = el.text_content().strip(":")
            sections.append(section)
    # Find sections by titles in text
    groupped = {}
    section = None
    for item in opis.itertext():
        item = item.strip(":")
        if item in sections:
            section = item
        elif section is not None:
            if section not in groupped:
                groupped[section] = []
            groupped[section].append(item)
    return groupped


def fix_number(s: str):
    s = s.replace(".", "")
    s = s.replace(",", ".")
    return s


numberify_functions = [
    lambda s: float(s),
    lambda s: float(fix_number(s)),
    lambda s: int(s),
    lambda s: int(fix_number(s))
]


def try_numberify(s: str):
    for func in numberify_functions:
        try:
            res = func(s)
            return res
        except ValueError:
            continue
    return s


def price_format(s):
    if s == "po dogovoru":
        return None
    if s == "Podarim":
        return float(0)
    s = s.strip(" €")
    s = fix_number(s)
    try:
        return float(s)
    except ValueError:
        return None


# datumify = lambda s: datetime.strptime(s, "%d.%m.%Y ob %H:%M")
datumify = lambda s: datetime.strptime(s.replace(":", ""), "%Y-%m-%dT%H%M%S%z")


def fixify(ad_info):
    if "m2" in ad_info["Cena"]:
        ad_info["cena_na_m2"] = True
        ad_info["Cena"] = ad_info["Cena"].rstrip(" / m2")
    ad_info["Cena"] = price_format(ad_info["Cena"])

    if "Bivalna površina" in ad_info:
        ad_info["Bivalna površina"] = float(fix_number(ad_info["Bivalna površina"].replace(" m²", "")))
    if "datum_objave" in ad_info:
        ad_info["datum_objave"] = datumify(ad_info["datum_objave"])


def read_ads(links, raw=False):
    for link in links:
        ad_info = read_ad(link, raw)
        yield ad_info
    print("")


async def read_ad(link:str=None, ad_id:int=None, raw:bool=False):
    if not link:
        link = f"https://www.bolha.com/?ctl=search_ads&keywords={ad_id}"
        async with aiohttp.request("GET", link) as resp:
            headers = resp.headers
        if "Location" not in headers:
            return None
        link = headers["Location"]

    bolha_id = link.split(".html")[0].split("-")[-1]
    print(bolha_id, end=" ")
    ad_info = {"link": link, "bolha_id": bolha_id}

    async with aiohttp.request("GET", link, allow_redirects=False) as resp:
        resp.raise_for_status()
        html = await resp.text()

    doc = lhtml.fromstring(html)
    ad = doc.cssselect(".ClassifiedDetail main")[0]
    if raw:
        global raw_ads
        raw_ads.append(ad)

    imgs = ad.cssselect(".ClassifiedDetailGallery-sliderListItem--image")
    img_urls = [img.get("data-thumb-image-url") for img in imgs]

    # Shitty table parsing
    for dl in ad.cssselect('dl'):
        for dt, dd in pairwise(dl.cssselect('dt,dd')):
            key = textify(dt).strip(': \t\n\r')
            ad_info[key] = textify(dd)

    title = textify(ad.cssselect('.ClassifiedDetailSummary-title')[0])
    desc = ad.cssselect(".ClassifiedDetailDescription-text")[0].text_content()

    fixify(ad_info)

    return Oglas(
        raw_info = ad_info,
        img_urls = img_urls,
        description=desc,
        price=ad_info['Cena'],
        title=title,
    )


def gimme_ads(path: str, params: dict = {}):
    ad_generator = read_ads(find_ad_links(fetch_pages(path, params)))
    for ad in ad_generator:
        yield ad
