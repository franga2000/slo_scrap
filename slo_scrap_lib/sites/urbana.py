from dataclasses import dataclass

from requests import Request

from slo_scrap_lib.fetcher import BaseFetcher


class Fetcher(BaseFetcher):
    pass

FETCHER = Fetcher()


@dataclass
class UrbanaPOI:
    bikeStatus: str = None
    id: int = None
    latitude: int = None
    longitude: int = None
    name: str = None
    nameNumber: str = None
    routes: str = None
    type: int = None
    zone: int = None


def get_pois():
    url = "https://mpsa.mestna.net/UrbanaAPI2/API2/map/"
    payload = {
        "count": 99999,
        "x1": 45000000,
        "x2": 47000000,
        "y1": 14000000,
        "y2": 15000000
    }
    resp = FETCHER.ensure_request(Request("POST", url, json=payload))
    pois = resp.json()['pois']
    for _poi in pois:
        poi = UrbanaPOI(**_poi['poi'])
        poi.type = _poi['type']
        poi.latitude /= 1000000
        poi.longitude /= 1000000
        yield poi

if __name__ == '__main__':
    pois = get_pois()
    print(list(pois))