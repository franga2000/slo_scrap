import asyncio
import logging
from datetime import datetime

import httpx
import zeep.wsdl.wsdl
from zeep import AsyncClient
from zeep.cache import SqliteCache
from zeep.transports import AsyncTransport

from slo_scrap_api import config_secret
from slo_scrap_lib.models import oJPP_Postaja, oJPP_LokacijaVozila

zeep.wsdl.wsdl.logger.setLevel(logging.INFO)
zeep.xsd.schema.logger.setLevel(logging.INFO)
logging.getLogger('httpcore.connection').setLevel(logging.INFO)

class IJPP:
    client: AsyncClient
    avl_client: AsyncClient

    async def init(self):
        cache = SqliteCache(path='/tmp/zepp_ijpp.db', timeout=60)
        httpx_client = httpx.AsyncClient(auth=(config_secret.IJPP.USERNAME, config_secret.IJPP.PASSWORD))
        httpx_sync_client = httpx.Client(auth=(config_secret.IJPP.USERNAME, config_secret.IJPP.PASSWORD))
        trans = AsyncTransport(client=httpx_client, wsdl_client=httpx_sync_client, cache=cache)
        self.client = AsyncClient(
            config_secret.IJPP.WSDL,
            transport=trans,
        )
        self.avl_client = AsyncClient(
            config_secret.IJPP.AVL_WSDL,
            transport=trans,
        )

    async def call_by_dict(self, method_name, request=dict(), avl=False):
        method = getattr((self.avl_client if avl else self.client).service, method_name)
        payload = dict(
            messageContext=dict(
                lRequestId=123,
            ),
            request=request,
        )
        resp = await method(**payload)
        return resp

    async def get_vehicle_locations_raw(self):
        resp = await self.call_by_dict('GetVehicleMonitoring', dict(
                RequestTimestamp=datetime.now().isoformat(),
                vehicleMonitoringRequest=dict(
                    version="2.0",
                    RequestTimestamp=datetime.now().isoformat(),
                )
            ),
           avl=True,
        )
        return resp.serviceDelivery.vehicleMonitoringDelivery.vehicleActivity.VehicleActivity

    async def get_stations(self):
        resp = await self.call_by_dict('GetPostajalisca')
        for p in resp.postajalisca.Postajalisce:
            if p.jeAktivno != 0:
                op = oJPP_Postaja(
                    id=p.idPostajalisce,
                    naziv=p.ime,
                    lat=p.geometrija.X,
                    lng=p.geometrija.Y,
                )
                yield op

    async def vozni_red(self, vstop_id, izstop_id, iso_datum):
        r1 = await self.call_by_dict('GetRelation', dict(
            iStartStationId=vstop_id,
            iEndStationId=izstop_id,
        ))
        ids = [r.idRelacija for r in r1.relacije.Relacija]
        r2 = await self.call_by_dict('GetRelacije', dict(
            idRelacije=ids
        ))
        return r2

    async def get_vehicle_locations(self):
        locations = []
        resp = await self.get_vehicle_locations_raw()
        for loc in resp:
            locations.append(oJPP_LokacijaVozila(
                vozilo_id=loc.monitoredVehicleJourney.VehicleRef,
                linija_id=loc.monitoredVehicleJourney.LineRef,
                prevoznik=loc.monitoredVehicleJourney.OperatorRef,
                lat=loc.monitoredVehicleJourney.vehicleLocation.Latitude,
                lng=loc.monitoredVehicleJourney.vehicleLocation.Longitude,
            ))


if __name__ == '__main__':
    async def main():
        ijpp = IJPP()
        await ijpp.init()

        #r1 = await ijpp.vozni_red(139129, 138922, None)

        #r1 = await ijpp.call_by_dict('GetPrevozniki')

        r2 = await ijpp.call_by_dict('GetVozniRediZaPrevoznika', dict(
            idPrevoznik=1109
        ))


        pass


    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
