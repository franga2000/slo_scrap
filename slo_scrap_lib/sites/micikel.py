import asyncio

import aiohttp
from geojson_pydantic import FeatureCollection, Point, Feature

from slo_scrap_lib.models import RideshareProvider, RidesharePointType, RideshareStation

async def get_stations():
    async with aiohttp.request("GET", 'https://api.micikel.bike/zemljevid/') as resp:
        data = await resp.json()

    features = []
    for station in data:
        point = Point(coordinates=(station['geodolzina'], station['geosirina']))
        feat = Feature(
            id=station['id'],
            geometry=point,
            properties=RideshareStation(
                title=station['postaja'],
                id=station['id'],
                type=RidesharePointType.BIKE_STATION,
                provider=RideshareProvider.MICIKEL,
                system="micikel",
                capacity=int(station['skupaj_mest']),
                capacity_free=int(station['prostih_mest']),
                vehicles_available=int(station['skupaj_mest']) - int(station['prostih_mest']),
                address=station['naslov'],
            ).dict()
        )
        features.append(feat)

    return FeatureCollection(features=features)


if __name__ == '__main__':
    async def main():
        ft = await get_stations()
        print(ft)

    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
