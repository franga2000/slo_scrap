import asyncio
from io import StringIO, BytesIO
from xml.etree.ElementTree import XMLParser

import aiohttp
import lxml.etree
from cache import AsyncTTL

from slo_scrap_api.config_secret import NEXTBIKE_API_KEY
from slo_scrap_lib.models import RideshareProvider, RidesharePointType, RideshareVehicle


API_BASE = 'https://api.nextbike.net/api'

#@cachier(stale_after=timedelta(minutes=2), next_time=True)
@AsyncTTL(time_to_live=60)
async def nextbike_live():
    url = "https://iframe.nextbike.net/maps/nextbike-live.xml"
    async with aiohttp.request("GET", url) as resp:
        return await resp.content.read()

@AsyncTTL(time_to_live=60*60*24)
async def nextbike_types():
    url = API_BASE + "/getBikeTypes.json?apikey=" + NEXTBIKE_API_KEY
    async with aiohttp.request("GET", url) as resp:
        resp = await resp.json()

    bike_types = {}
    for i, bike in enumerate(resp['items']):
        bike_types[str(bike['uid'])] = bike
    return bike_types


parser = XMLParser()


async def get_stations(country='SI'):
    tree = lxml.etree.iterparse(BytesIO(await nextbike_live()), tag='country', huge_tree=True)
    features = []

    for action, el in tree:
        if el.attrib['country'] != country:
            el.clear()
            continue
        provider = el

        for place in provider.xpath('.//place'):
            if 'bike_racks' in place.attrib:
                capacity = int(place.attrib.get('bike_racks'))
            elif 'bikes_available_to_rent' in place.attrib:
                capacity = int(place.attrib.get('bikes_available_to_rent')) + int(place.attrib['free_racks'])
            else: continue

            # NOTE: constructing this using Pydantic is like 10x slower, so we're just hand-rolling the JSON
            # feat = Feature(
            #     id=place.attrib['uid'],
            #     geometry=Point(coordinates=(place.attrib['lng'], place.attrib['lat'])),
            #     properties=RideshareStation(
            #         title=place.attrib['name'],
            #         id=place.attrib['uid'],
            #         type=RidesharePointType.BIKE_STATION,
            #         system=RideshareSystem.NOMAGO_BIKES,
            #         capacity=capacity,
            #         capacity_free=place.attrib['free_racks'],
            #     )
            # )
            feat = {
                "id": place.attrib['uid'],
                "geometry": {
                    "type": "Point",
                    "coordinates": [place.attrib['lng'], place.attrib['lat']]
                },
                "properties": {
                    "title": place.attrib['name'],
                    "id": place.attrib['uid'],
                    "type": RidesharePointType.BIKE_STATION,
                    "provider": RideshareProvider.NOMAGO_BIKES,
                    "system": provider.attrib['name'],
                    "capacity": capacity,
                    "capacity_free": place.attrib['free_racks'],
                    "vehicles_available": place.attrib['bikes'],
                    "deeplink": f"https://app.nextbike.net/station?number={place.attrib.get('number')}",
                }
            }
            features.append(feat)
    return {
        "type": "FeatureCollection",
        "features": features
    }
    # return FeatureCollection(features=features)


async def get_vehicles(station_id:str):
    tree = lxml.etree.iterparse(BytesIO(await nextbike_live()), tag='place', huge_tree=True)
    bike_types = await nextbike_types()

    bikes = []
    for action, place in tree:
        if place.attrib['uid'] != station_id:
            place.clear()
            continue

        domain = place.getparent().getparent().attrib['domain']

        for bike in place.xpath('.//bike'):
            b = RideshareVehicle(
                id=bike.attrib['number'],
                image_url=f"https://static.nextbike.net/app/biketypes/type/{domain}/{bike.attrib['bike_type']}/h200.png",
                deeplink='https://app.nextbike.net/bike/' + str(bike.attrib['number']),
            )
            battery = bike.xpath('.//battery_pack')
            if battery and 'percentage' in battery[0].attrib:
                b.charge_level = int(battery[0].attrib.get('percentage'))
            model = bike_types.get(bike.attrib['bike_type'])
            if model:
                b.model = model['name']
            bikes.append(b)

    return bikes


if __name__ == '__main__':
    async def main():
        ft = await get_stations('SI')
        print(ft['features'])
        #print(ft)

    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
