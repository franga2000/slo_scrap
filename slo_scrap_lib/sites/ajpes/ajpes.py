import asyncio
import dataclasses

import aiohttp
from bs4 import BeautifulSoup, Tag

from slo_scrap_lib.html_util import textify


@dataclasses.dataclass
class AjpesLink:
    _ajpes: 'AJPES'
    firma: str
    id: int


class AJPES:
    BASE = 'https://www.ajpes.si'

    async def init(self):
        self.session = aiohttp.ClientSession()

    async def _fetch(self, url, params=None, body=None):
        for _ in range(2):
            method = 'POST' if body else 'GET'

            resp = await self.session.request(method, url, params=params, data=body)
            txt = await resp.text()

            if 'PRIJAVA V SISTEM' in txt:
                u = f'{self.BASE}/MDScripts/Ajax.asp?method=checkuserAnonim'
                resp = await self.session.get(u)
                resp.close()

            elif 'formCaptcha' in txt:
                captcha_url = None  # TODO: captcha URL
                code = await self._solve_captcha(captcha_url)
                body = f'jolp_koda={code}&izjava_strinjam=1'
            else:
                break
        return txt

    def search(self, **kwargs) -> [AjpesLink]:
        pass

    async def srg_info(self, prs_id):
        url = f'{self.BASE}/prs/podjetjeSRG.asp?s=1&e={prs_id}'
        html = await self._fetch(url)
        return html

    async def prs_info(self, prs_id):
        url = f'{self.BASE}/prs/podjetje.asp?s=1&e={prs_id}&p=1'
        html = await self._fetch(url)
        dom = BeautifulSoup(html)
        main = dom.select_one('#main section .col-lg-10')

        data = {}
        current = None
        for el in main.children:
            el: Tag
            if el.name == 'h4':
                current = {}
                data[textify(el)] = current

            elif el.name == 'div':
                last = None
                for row in el.select('.col-md-12'):
                    ch = [c for c in row.children if type(c) == Tag]
                    key = textify(ch[0]).strip(":")
                    val = textify(ch[1])

                    if not key:
                        key = last
                    last = key
                    if key in current:
                        if type(current[key]) != list:
                            current[key] = [current[key]]
                        current[key].append(val)
                    else:
                        current[key] = val
        return data

    async def letna_porocila(self, maticna):
        url = f'{self.BASE}/jolp/podjetje.asp?maticna={maticna}'
        html = await self._fetch(url)
        return html


if __name__ == '__main__':
    async def main():
        ajpes = AJPES()

        margento = await ajpes.prs_info(368463)
        print(margento)

        await ajpes.session.close()

    asyncio.run(main())
