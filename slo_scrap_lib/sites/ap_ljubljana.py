import asyncio
import re
from datetime import date

import aiohttp
from bs4 import BeautifulSoup, Tag
from dateutil.parser import isoparse
from requests import Request

from slo_scrap_lib.fetcher import AutoProxyFetcher
from slo_scrap_lib.models import oJPP_Vožnja


async def seznam_postaj():
    # TODO: this is broken, fix it
    async with aiohttp.ClientSession() as session:
        resp = await session.get("https://www.ap-ljubljana.si/vozni-red/")
        html = BeautifulSoup(await resp.text())

        script_src = next(filter(lambda el: "chunk-common." in el.attrs["src"], html.select("script[type=module]"))).attrs['src']
        resp = await session.get("https://www.ap-ljubljana.si" + script_src)
        js = await resp.text()

    start = js.find('["LJUBLJANA AVTOBUSNA POSTAJA') + 1
    end = resp.text[start:].find(']')
    ls = [n.strip() for n in resp.text[start:start+end].replace('"', '').split(',')]

    return {n:n for n in ls}


DURATION_RE = re.compile(r'(?:(\d+) ur )?(\d+) minut')

async def vozni_red(vstop_id: str, izstop_id: str, iso_datum: str, include_details=False):
    dat = isoparse(iso_datum)
    url = "https://www.ap-ljubljana.si/vozni-red"
    params = {
        "vstop": str(vstop_id),
        "izstop": str(izstop_id),
        "datum": iso_datum,
    }
    async with aiohttp.request('GET', url, params=params) as resp:
        html = BeautifulSoup(await resp.text())

    vožnje = []

    container = html.select_one("#timetable-header ~ div.flex-col.mx-auto")

    for ride_el in container.children:
        ride_el: Tag

        if "Mednarodn" in ride_el.text:
            # trans = json.loads(unescape(ride_el.attrs[':translations']))
            continue

        hr, mi = ride_el.attrs['cas-prihoda'].split(':')
        prihod = dat.replace(hour=int(hr), minute=int(mi))

        hr, mi = ride_el.attrs['cas-odhoda'].split(':')
        odhod = dat.replace(hour=int(hr), minute=int(mi))

        # hours, mins = DURATION_RE.search(ride_el.attrs['cas-voznje']).groups()
        # duration = (int(hours or 0)*60 + int(mins or 0)) * 60

        ride_id = ride_el.attrs['request-details-url'].rsplit('/', maxsplit=1)[-1]

        v = oJPP_Vožnja(
            odhod=odhod,
            prihod=prihod,
            more_detail=ride_id,
            id=ride_id,
        )
        vožnje.append(v)

    if include_details:
        details = await asyncio.gather(*[ride_detail(v.id, session) for v in vožnje])
        for v, d in zip(vožnje, details):
            v.naziv = d.naziv
            v.prevoznik = d.prevoznik
    await session.close()

    return vožnje


async def ride_detail(ride_id: str, session) -> oJPP_Vožnja:
    url = f"https://www.ap-ljubljana.si/vozni-red/odhod_details_ajax/aplj/{ride_id}"
    resp = await session.get(url)
    html = BeautifulSoup(await resp.text())

    frame = html.select_one(".border-t.py-4")

    name = frame.find(lambda x: x.text.startswith("Linija")).text[8:]
    prevoznik = frame.select_one(".justify-between").text

    vožnja = oJPP_Vožnja(
        naziv=name,
        prevoznik=prevoznik,
    )
    return vožnja


if __name__ == '__main__':
    async def main():
        #sp = await seznam_postaj()
        #print(len(sp))
        #with open("../../aplj.csv", "w") as f:
        #    f.write("")
        vr = await vozni_red(10783, 8537, "2023-06-13")
        print(vr)
        print()
        vd = ride_detail(vr[0].id)
        print(vd)

    asyncio.run(main())
