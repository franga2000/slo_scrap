import json

import aiohttp
from bs4 import BeautifulSoup

import time

TRIGLAV_JSESSIONID = ""

TRIGLAV_HEADERS = {
    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:108.0) Gecko/20100101 Firefox/108.0",
    "Referer": "https://skleni.triglav.si/isklepanje/avto/avto_zavarovanje_1",
    "Content-Type": "application/x-www-form-urlencoded",
    "Origin": "https://skleni.triglav.si",
}


async def get_triglav(plate, vin):
    global TRIGLAV_JSESSIONID
    for _ in range(3):
        try:
            # If JSESSIONID is not set, get it
            if not TRIGLAV_JSESSIONID:
                raise Exception("JSESSIONID not set")

            # Get vehicle data from triglav
            async with aiohttp.request(
                    'POST',
                    f'https://skleni.triglav.si/isklepanje/avto/searchVehicleData',
                    data={"vinNumber": vin, "regnumNumber": plate},
                    cookies={"JSESSIONID": TRIGLAV_JSESSIONID},
                    headers=TRIGLAV_HEADERS,
            ) as triglav_response:
                if triglav_response.status != 200:
                    return {"success": False, "error": "Triglav API error"}

                vehicle_data = BeautifulSoup(await triglav_response.text(), "html.parser")

            inputs = vehicle_data.select("input")
            data = {
                'success': True,
            }
            for input in inputs:
                text_label = input.parent.text.strip()
                label = input.attrs.get("name", input.attrs.get("id", text_label))
                value = input.attrs["value"]
                data[label] = value
            return data

        except Exception:
            async with aiohttp.request(
                    'GET',
                    'https://skleni.triglav.si/isklepanje/avto/avto_zavarovanje_1',
                    headers=TRIGLAV_HEADERS,
            ) as triglav_response:
                if triglav_response.status != 200:
                    return {'success': False, 'error': "Triglav API error (can't get cookie)"}

            # If response is 200, get JSESSIONID cookie
            TRIGLAV_JSESSIONID = triglav_response.cookies.get("JSESSIONID")

            await asyncio.sleep(2)
    return {'success': False, 'error': 'Triglav API error (retry limit reached)'}


CSRF_TOKEN = ""
UPDATED_AT = 0

AVTOLOG_HEADERS = {
    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:108.0) Gecko/20100101 Firefox/108.0",
    "Referer": "https://avtolog.si/",
    "Origin": "https://avtolog.si",
}


async def get_avtolog(vin):
    global CSRF_TOKEN, UPDATED_AT
    # Token refresh if necessary
    if CSRF_TOKEN == "" or UPDATED_AT + 3600 < time.time():
        async with aiohttp.request('GET', 'https://www.avtolog.si/', headers=AVTOLOG_HEADERS) as r:
            r.raise_for_status()
            CSRF_TOKEN = r.cookies.get("csrftoken")
            UPDATED_AT = time.time()

    async with aiohttp.request('GET', f"https://avtolog.si/search/{vin}/", headers=AVTOLOG_HEADERS) as avtolog_response:
        if avtolog_response.status != 200:
            return {"success": False, "error": "Avtolog API error"}

        vehicle_data = BeautifulSoup(await avtolog_response.text(), "html.parser")
        scripts = vehicle_data.select('script[type="application/json"]')

        data = {
            "user": json.loads(scripts[0].text if scripts[0].text != "" else 'null'),
            "km_data": json.loads(scripts[1].text if scripts[1].text != "" else 'null'),
            "uporabnik_starost": json.loads(scripts[2].text if scripts[2].text != "" else 'null'),
            "lastnik_starost": json.loads(scripts[3].text if scripts[3].text != "" else 'null'),
            "car_data": json.loads(scripts[4].text.replace('"mrvl": \n', '"mrvl":null') if scripts[4].text != "" else 'null'),
        }
        return data


EVINJETA_HEADERS = {
    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:108.0) Gecko/20100101 Firefox/108.0",
    "Referer": "https://evinjeta.info/",
    "Origin": "https://evinjeta.info",
}


async def get_evinjeta(registration_number):
    async with aiohttp.request('POST', 'https://evinjeta.info/', data={"registrska": registration_number}, headers=EVINJETA_HEADERS) as resp:
        if resp.status != 200:
            return {"success": False, "error": "Evinjeta API error"}
        vehicle_data = BeautifulSoup(await resp.text(), "html.parser")

    # get b elements - if there is only one, it means that the vehicle is not registered
    b_elements = vehicle_data.select(".message b")
    if len(b_elements) <= 1:
        return {"has_evinjeta": False}
    else:
        return {"has_evinjeta": True, "date": b_elements[1].text}


async def car_data(plate="", vin=""):
    plate = plate.upper().replace(" ", "").replace("-", "") if plate != None else ""
    vin = vin.upper().replace(" ", "").replace("-", "") if vin != None else ""
    if not plate and not vin:
        return {"success": False, "error": "No plate or vin provided"}

    # Get vehicle data
    vehicle_data_triglav = await get_triglav(plate, vin)
    if vehicle_data_triglav["success"] == False or vehicle_data_triglav['vehiclePolicy.vehicle.vehicleIdentificationNumber'] == '':
        return {
            "success": False,
            "error": "Vehicle data not found",
        }
    vehicle_data_avtolog = await get_avtolog(vehicle_data_triglav["vehiclePolicy.vehicle.vehicleIdentificationNumber"])
    vehicle_data_evinjeta = await get_evinjeta(vehicle_data_triglav["vehiclePolicy.vehicle.vehicleRegistrationNumber"])

    # If vehicle data is not empty, return it
    data = {
        "success": True,
        "data": {
            "registration_number": vehicle_data_triglav[
                "vehiclePolicy.vehicle.vehicleRegistrationNumber"
            ],
            "car": {
                "manufacturer": vehicle_data_triglav["carManufacturer"],
                "model": vehicle_data_avtolog["car_data"]["model"],
                "type": vehicle_data_triglav["carType"],
                "vin": vehicle_data_triglav[
                    "vehiclePolicy.vehicle.vehicleIdentificationNumber"
                ],
                "fuel_type": vehicle_data_avtolog["car_data"]["gorivo"],
                "color": vehicle_data_avtolog["car_data"]["barva"],
                "year": vehicle_data_avtolog["car_data"]["year"] if vehicle_data_avtolog["car_data"]["year"] != "None" else None,
                "owner_count": int(vehicle_data_avtolog["car_data"]["owner_count"]),
            },
            "registration": {
                "first_registration": {
                    "date": vehicle_data_avtolog["car_data"]["datum_prve_reg"] if vehicle_data_avtolog["car_data"]["is_imported"] == "True" else vehicle_data_avtolog["car_data"]["datum_prve_reg_slo"],
                    "date_slo": vehicle_data_avtolog["car_data"]["datum_prve_reg_slo"],
                },
                "km": vehicle_data_avtolog["car_data"]["km"],
                "is_mot": vehicle_data_avtolog["car_data"]["has_tehnicni"] == "True",
                "is_stolen": vehicle_data_avtolog["car_data"]["is_stolen"] == "True",
            },
            "vignette": {
                "has_evinjeta": vehicle_data_evinjeta["has_evinjeta"],
                "date": vehicle_data_evinjeta["date"] if vehicle_data_evinjeta["has_evinjeta"] else None,
            },
        },
    }
    return data


if __name__ == '__main__':
    import asyncio

    loop = asyncio.get_event_loop()
    #vinjeta = loop.run_until_complete(get_evinjeta('LJ MOMMY'))
    #triglav = loop.run_until_complete(get_triglav('LJ 33-KSU'))
    #avtolog = loop.run_until_complete(get_avtolog('LJ MOMMY'))
    data = loop.run_until_complete(car_data('LJ MOMMY'))
    print(data)
