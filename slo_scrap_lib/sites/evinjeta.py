import asyncio
import datetime
import pickle

import aiohttp

from slo_scrap_api.util import repeat_every


headers = {
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:100.0) Gecko/20100101 Firefox/100.0'
}

session: aiohttp.ClientSession = None


async def setup():
    global session
    session = aiohttp.ClientSession(cookie_jar=aiohttp.CookieJar())
    with open('data/evinjeta.pickle', 'rb') as f:
        session.cookie_jar.update_cookies(pickle.load(f))


async def validate(tablica):
    global session
    start = datetime.datetime.now() + datetime.timedelta(days=1)
    end = start + datetime.timedelta(days=265)

    # get XSRF token
    r = await session.get("https://evinjeta.dars.si/selfcare/api/server/settings", headers=headers, ssl=False)

    headers['X-XSRF-Token'] = r.headers['x-xsrf-token']

    r = await session.post("https://evinjeta.dars.si/selfcare/api/eshop/shopping-cart/validate", headers=headers, ssl=False, json={
        "tollClassId": 2,
        "validityDefinitionId": 29,
        "vignetteCategoryId": 4,
        "vignetteTypeId": 1,
        "vignetteValidityStart": start.isoformat(),
        "vignetteValidityEnd": end.isoformat(),
        "shoppingSessionId": 1,
        "registrationCountryCode": "SI",
        "registrationNumber": tablica
    })

    return await r.json()


#@repeat_every(seconds=60)
async def refresh():
    r = await session.get("https://evinjeta.dars.si/selfcare/api/account/testlogin", headers=headers, ssl=False)

    with open('cookiejar.pickle', 'wb') as f:
        pickle.dump({c.key: c.value for c in session.cookie_jar}, f)



if __name__ == '__main__':
    async def main():
        ft = await validate('LJ32VCJ')
        print(ft)


    loop = asyncio.get_event_loop()
    loop.run_until_complete(setup())
    loop.run_until_complete(main())
