import asyncio
import datetime
import json
import time
from csv import DictWriter

import aiohttp

from slo_scrap_lib.models import oJPP_Vožnja, oJPP_Postaja


TOKEN = None
TOKEN_EXPIRY = None


async def get_api_key():
    global TOKEN, TOKEN_EXPIRY

    if TOKEN and TOKEN_EXPIRY > time.time():
        return TOKEN

    js = {
        "request": {
            "AppVersion": "2.4.9",
            "DeviceDescription": "Android 69/iPhone S23 Ultra Max Pro",
            "DeviceId": "dead000beef",
            "PlatformId": 2,
            "PushId": "0",
            "Culture": "sl-SI"
        },
        "messageContext": {
            "RequestId": 1654721260946
        }
    }
    async with aiohttp.request("POST", 'https://ijpp.nomago.si/MobileAPI/Authentication/', json=js) as resp:
        resp.raise_for_status()
        resp = await resp.json()
    assert resp['ResultCode'] == 0, resp['ResultDescription']

    TOKEN_EXPIRY = datetime.datetime.strptime(resp['TokenValidTo'], '%Y-%m-%d %H:%M:%S').timestamp()
    TOKEN = resp['Token']

    return TOKEN


async def seznam_postaj():
    # api_data = await get_api_key()
    #
    # now = int(time.time())
    # params = {
    #     "request": {
    #         "TimeStamp": f"/Date({now})/",
    #         "Culture": "sl-SI"
    #     },
    #     "messageContext": {
    #         "AuthToken": api_data['Token'],
    #         "RequestId": now
    #     }
    # }
    async with aiohttp.request('POST', 'https://ijpp.nomago.si/MobileAPI/GetStations/', json={}) as resp:
        resp = json.loads(await resp.content.read())

    postaje = [oJPP_Postaja(
        id=p['StationId'],
        naziv=p['StationName'],
        lat=p['Latitude'],
        lng=p['Longitude'],
    ) for p in resp['Stations']]

    return postaje


async def vozni_red(vstop_id: str, izstop_id: str, iso_datum: str):
    token = await get_api_key()

    date = int(datetime.datetime.strptime(iso_datum, '%Y-%m-%d').replace(hour=10).timestamp()*1000)
    params = {
        "request": {
            "date": [
                f"/Date({date})/"
            ],
            "endStationId": izstop_id,
            "isReverse": False,
            "products": [
                {
                    "count": 1,
                    "CUID": 0,
                    "NumberOfTickets": 0,
                    "productId": 2102,
                    "ProductName": "Vozovnica odrasli",
                    "Project": 0,
                    "totalCount": 0
                }
            ],
            "startStationId": vstop_id,
            "Culture": "sl-SI"
        },
        "messageContext": {
            "AuthToken": token,
            "RequestId": int(time.time()*1000)
        }
    }

    async with aiohttp.request('POST', 'https://ijpp.nomago.si/MobileAPI/GetRoutesAdvanced/', json=params) as resp:
        resp.raise_for_status()
        resp = json.loads(await resp.content.read())

    vožnje = []

    for p in resp['routes']:
        v = oJPP_Vožnja(
            odhod=datetime.datetime.fromisoformat(p['tStartTime']),
            prihod=datetime.datetime.fromisoformat(p['tEndTime']),
            id=p['iTripId'],
            razdalja=(p['distance'] / 1000) if p['distance'] > 1000 else p['distance'],
            prevoznik=p['sBusOperator'],
        )
        if v.prevoznik != "Nomago d.o.o.":
            v.opozorila = ['Podatki vira Nomago za druge prevoznike niso nujno točni']
        if p['products']:
            v.cena = p['products'][0]['productPrice']
        vožnje.append(v)
    return vožnje


if __name__ == '__main__':
    async def main():
        vz = await vozni_red(139129, 138922, datetime.date.today().isoformat()[0:10])
        print(vz)

        ft = await seznam_postaj()
        print(ft)
        with open('data/nomago_stations.csv', 'w') as f:
            w = DictWriter(f, fieldnames=oJPP_Postaja.schema()['properties'].keys())
            w.writeheader()
            for s in ft:
                w.writerow(s.dict())

    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
