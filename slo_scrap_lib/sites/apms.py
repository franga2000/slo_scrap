import asyncio
import json
import re
from datetime import datetime

from slo_scrap_lib.fetcher import session
from slo_scrap_lib.models import oJPP_Vožnja
from slo_scrap_lib.util import hour_on_date


JJ_RE_1 = re.compile(r'([\{])(.+?):')
JJ_RE_2 = re.compile(r'", ?(.+?):')
def js_to_json(js):
    return JJ_RE_2.sub(r'","\1":', JJ_RE_1.sub(r'\1"\2":', js))


async def seznam_postaj():
    async with (await session()).request("GET", 'https://www.apms.si/voznired', ssl=False) as resp:
        resp.raise_for_status()
        resp = await resp.text()
    js = resp.split('var postajalisca = ', maxsplit=1)[-1].split(';', maxsplit=1)[0]
    js = js_to_json(js)
    postaje = json.loads(js)
    return {p['value']: p['label'] for p in postaje}


async def vozni_red(vstop_id: str, izstop_id: str, iso_datum: str):
    datum = datetime.strptime(iso_datum, "%Y-%m-%d")
    slo_datum = datum.strftime("%d.%m.%Y")
    data = {
        'datum': slo_datum,
        'postaja_od': vstop_id,
        'postaja_do': izstop_id,
    }
    async with (await session()).request("POST", 'https://www.apms.si/response.ajax.php?com=voznired2020&task=get', data=data, ssl=False) as resp:
        resp.raise_for_status()
        js = await resp.json()

    vožnje = []

    for vožnja in js:
        v = oJPP_Vožnja(
            prihod=hour_on_date(datum, vožnja["prihod"]),
            odhod=hour_on_date(datum, vožnja["odhod"]),
            razdalja=vožnja["km"],
            prevoznik=vožnja["prevoznik"],
            cena=float(vožnja['cena'].strip()),
        )
        vožnje.append(v)

    return vožnje

if __name__ == '__main__':
    ft = asyncio.get_event_loop().run_until_complete(vozni_red("Ljubljana AP", "Maribor AP", iso_datum='2023-03-06'))
    print(ft)
    pass
