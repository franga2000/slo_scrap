import asyncio
from collections import namedtuple
from datetime import datetime, timedelta

import aiohttp
from cache import AsyncLRU, AsyncTTL

from slo_scrap_api.config_secret import GMAPS_API_KEY
from slo_scrap_lib.models import oJPP_Vožnja

RouteInfo = namedtuple('RouteInfo', ('distance', 'duration',))


@AsyncLRU(maxsize=None)
async def gmaps_route(start, dest):
    url = f'https://maps.googleapis.com/maps/api/directions/json?origin={start}&destination={dest}&key={GMAPS_API_KEY}'
    async with aiohttp.request("GET", url) as resp:
        data = await resp.json()
    leg = data['routes'][0]['legs'][0]
    return RouteInfo(leg['distance']['value'], leg['duration']['value'])


@AsyncTTL(time_to_live=60)
async def vsi_prevozi(date_iso):
    url = f"https://prevoz.org/api/search/shares/?f=&fc=SI&t=&tc=SI&d={date_iso}&exact=false&intl=false"
    async with aiohttp.request("GET", url) as resp:
        return await resp.json()


async def vozni_red(vstop_id: str, izstop_id: str, iso_datum: str):
    vr = await vsi_prevozi(iso_datum)

    vožnje = []
    for prevoz in vr['carshare_list']:
        if prevoz['from'] != vstop_id or prevoz['to'] != izstop_id or not prevoz['date_iso8601'].startswith(iso_datum):
            continue

        route = await gmaps_route(vstop_id, izstop_id)
        odhod = datetime.fromisoformat(prevoz['date_iso8601'])
        v = oJPP_Vožnja(
            odhod=odhod,
            prihod=odhod + timedelta(seconds=route.duration),
            id=prevoz['id'],
            razdalja=(route.distance / 1000) if route.distance > 1000 else route.distance,
            cena=prevoz['price'],
            prevoznik='Prevoz.org: ' + prevoz['author'],
        )
        vožnje.append(v)
    return vožnje


if __name__ == '__main__':
    async def main():
        ft = await vozni_red('Maribor', 'Ljubljana', datetime.today().isoformat()[0:10])
        print(ft)

    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
