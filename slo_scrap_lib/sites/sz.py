import asyncio
import json
import logging
from datetime import datetime, timedelta, date
from typing import Dict
from urllib.parse import urlencode

import aiohttp
import bs4
from bs4 import BeautifulSoup
from cache import AsyncTTL
from cashews import cache

from slo_scrap_api import config_secret
from slo_scrap_lib.html_util import textify, select_to_dict, table_to_dict_list
from slo_scrap_lib.models import oJPP_Vožnja, SZ_Lokacija, oJPP_Postaja, SZ_TrainDetails, SZ_TrainDetailsStop
from slo_scrap_lib.util import hour_on_date

ESHOP_BASE_URL = config_secret.SZ_ESHOP_PROXY or 'https://eshop.sz.si'
POTNISKI_BASE_URL = config_secret.SZ_POTNISKI_PROXY or 'https://potniski.sz.si'


async def sz_request(method, url, **kwargs):
    url = 'https://potniski.sz.si' + url
    payload = {
        'cmd': 'request.' + method.lower(),
        'url': url + '?' + urlencode(kwargs.get('params', {})),
    }
    if method == 'POST':
        payload['postData'] = urlencode(kwargs.get('data', {}))
    
    async with aiohttp.request('POST', config_secret.FLARESOLVER_URL + '/v1', json=payload) as resp:
        resp.raise_for_status()
        r = await resp.json()
        return r['solution']['response']


async def seznam_postaj():
    txt = await sz_request('GET', '/vozni-red/?action=timetables_search')
    #html = BeautifulSoup(txt)
    lines = txt.splitlines()
    for line in lines:
        if line.startswith('var timetablesData = '):
            data = json.loads(line[21:-1])
            break
    #return select_to_dict(html.find('select', id='entry-station'))
    stations = json.loads(data['trainStations'])

    postaje = [oJPP_Postaja(
        id=p['st'],
        naziv=p['naziv'],
        lat=float(p['Geo_sirina']) if p['Geo_sirina'] else None,
        lng=float(p['Geo_dolzina']) if p['Geo_dolzina'] else None,
    ) for p in stations]

    return postaje

seznam_postaj_cached = AsyncTTL(60*60*24)(seznam_postaj)


async def vozni_red(vstop_id: int, izstop_id: int, iso_datum: str):
    today = datetime.strptime(iso_datum, "%Y-%m-%d")
    slo_date = today.strftime("%d.%m.%Y")
    params = {
        'action': 'timetables_search',
        'current-language': 'sl',
        'departure-date': slo_date,
        'entry-station': vstop_id,
        'exit-station': izstop_id,
    }
    txt = await sz_request('GET', '/vozni-redi-results/', params=params)
    html = BeautifulSoup(txt)
    vožnje = set()
    for row in html.select("#connections-output .train"):
        train_list = [json.loads(b.attrs['data-transit-station-data']) for b in row.select(".get-middle-stations[data-transit-station-data]")]
        trains = {}
        for lst in train_list:
            for train in lst:
                trains[train['vlak']] = train

        train_names = list(trains.keys())
        fr_datetime = hour_on_date(today, trains[train_names[ 0]]['departure']['time'])
        to_datetime = hour_on_date(today, trains[train_names[-1]]['maxtime'])

        # Handle next day
        if fr_datetime > to_datetime:
            to_datetime += timedelta(days=1)
        v = oJPP_Vožnja(
            odhod=fr_datetime,
            prihod=to_datetime,
            naziv=' - '.join(train_names),
        )
        for warning in row.select('.sub-wrapper .get-transit-stations'):
            txt = textify(warning).replace(' Več:', '')
            v.opozorila.append(txt)
        vožnje.add(v)
    return vožnje


async def train_details(train, iso_date):
    date = datetime.strptime(iso_date, "%Y-%m-%d")
    txt = await sz_request('POST', '/wp-admin/admin-ajax.php', data={
        'action': 'train_details',
        'data[train]': train,
        'data[date]': date.strftime('%d.%m.%Y'),
    })
    # Strip JSON encoding
    txt = json.loads(txt)

    html = BeautifulSoup(txt)
    details = SZ_TrainDetails(
        date=date,
    )

    for strong in html.select('span.strong'):
        txt = strong.text.strip()
        if 'Vozni red' in txt:
            details.timetable_description = txt
        elif 'vozi' in txt.lower():
            details.validity_description = txt

    try:
        stop_map: Dict[str, oJPP_Postaja] = {stop.naziv: stop for stop in await seznam_postaj_cached()}
    except Exception as e:
        logging.exception(e)
        stop_map = {}

    stops = table_to_dict_list(html.select_one('table'))
    for stop in stops:
        st = stop_map.get(stop['Postaja'])
        if not st:
            continue
        details.stops.append(SZ_TrainDetailsStop(
            id=st.id,
            name=stop['Postaja'],
            arrival=stop['Prihod'],
            departure=stop['Odhod'],
        ))

    return details


def get_and_log(dict, key, default=None):
    try:
        return dict[key]
    except KeyError:
        logging.exception(f'Missing key: {key}', extra=dict)
        return default

async def potniski_lokacije():
    #data = await potniski_lokacije_raw()
    data = await sz_request('POST', '/wp-admin/admin-ajax.php', data={'action': 'aktivni_vlaki'})
    # Sometimes the JSON is wrapped in HTML tags?? Probably a flaresolver bug
    data = data.replace('<html><head></head><body>', '').replace('</body></html>', '')
    data = json.loads(data)

    # If only one train is active, the API returns a dict instead of a list
    if type(data) != list:
        data = [data]

    locations = []
    for vlak in data:
        if not vlak['Koordinate']:
            continue
        lon, lat = vlak['Koordinate'].split(',')
        loc = SZ_Lokacija(
            st_vlaka=get_and_log(vlak, 'St_vlaka'),
            rang_vlaka=get_and_log(vlak, 'Rang'),
            lat=float(lat),
            lng=float(lon),
            relacija=get_and_log(vlak, 'Relacija'),
            naslednja_postaja=get_and_log(vlak, 'Postaja'),
            postaja_eta_min=int(vlak['Predviden_prihod']),
            zamuda_min=int(vlak['Zamuda_cas']),
            bus=get_and_log(vlak, 'Vrsta_vlaka', 'V') != 'V',
            raw_odhod=get_and_log(vlak, 'Odhod'),
            raw_st=get_and_log(vlak, 'St'),
        )
        locations.append(loc)
    return locations


async def eshop_seznam_postaj():
    url = ESHOP_BASE_URL + '/Home/TravelAutocomplete'
    map = {}
    for letter in (chr(n) for n in range(ord("a"), ord("z") + 1)):
        data = {"SearchText": letter}
        async with aiohttp.request("POST", url, data=data) as resp:
            html = BeautifulSoup(await resp.text())
        for li in html.select("li"):
            map[li.attrs["data-value"]] = li.attrs["data-text"]
    return map

async def eshop_vozni_red(vstop_id: str, izstop_id: str, iso_datum: str):
    datum = datetime.strptime(iso_datum, "%Y-%m-%d")
    slo_datum = datum.strftime("%d.%m.%Y")
    url = ESHOP_BASE_URL + "/Home/FilterSingleListing"
    vožnje = set()

    for icmcvc in ("true", "false",):
        data = {
            "SubSelect": 0,
            "TicketDirection": "OneWay",
            "DepartureDate": slo_datum,
            # ReturnDate": "12.11.2021",
            #"NumOfPassengersPlaceholder": "1x+Redna+cena",
            "NumOfPassengers[0]": 1,
            "NumOfPassengers[1]": 0,
            "NumOfPassengers[2]": 0,
            # "TravelFrom": "Maribor",
            "TravelFromId": vstop_id,
            "TravelToId": izstop_id,
            # "TravelTo": "Ljubljana",
            "IcMcVc": icmcvc,
            "NoTransfer": "false",
            # NumOfTouristsPlaceholder": "1x+Turist+vikend",
            # TravelFrom": "",
            # TravelFromId": "0",
            # TravelFrom": "",
            # TravelTo": "",
            # TravelToId": "0",
            # TravelTo": "",
            "SeatClass": "SecondClass",
        }
        async with aiohttp.request("POST", url, data=data) as resp:
            html = BeautifulSoup(await resp.text())

        for el in html.select(".filter-table tr td"):
            fr, to = el.select(".filter-route .go-medium")
            fr, to = fr.text, to.text
            name = textify(el.select(".filter-route-inner")[-1])
            odhod = hour_on_date(datum, fr)
            prihod = hour_on_date(datum, to)
            if odhod > prihod:
                prihod += timedelta(days=1)
            v = oJPP_Vožnja(odhod=odhod, prihod=prihod, naziv=name)
            vožnje.add(v)
    return vožnje


cache.setup("disk://?directory=data&shards=0", size_limit=1073741824)


@cache(ttl='10000h', key='oid:{oid_enc}')
async def karta_info(oid_enc):
    async with aiohttp.ClientSession() as session:
        url = f'{ESHOP_BASE_URL}/sl/BankCard/PaymentOK?receiptId={oid_enc}&genericProduct=True'
        resp = await session.get(url)
        resp.raise_for_status()
    html = bs4.BeautifulSoup(await resp.content.read())
    dt = datetime.strptime(html.select_one('.receipt-customer p').text, '%d. %m. %Y, %H:%M:%S')
    try:
        r_od, r_do = (textify(html.find('label', string=s + ' postaja').parent.parent.select_one('.go-medium')) for s in ('Vstopna', 'Izstopna',))
    except AttributeError:
        r_od = r_do = None

    return r_od, r_do, dt



if __name__ == '__main__':
    async def main():

        locs = await potniski_lokacije()

        pass

        #postaje = await seznam_postaj()

        # det = await train_details('2268', date.today().isoformat()[0:10])

        #v1 = await vozni_red(42300, 43400, date.today().isoformat()[0:10])


        #v2 = await eshop_vozni_red(136864, 136755, date.today().isoformat()[0:10])
        #print(v2)

    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
