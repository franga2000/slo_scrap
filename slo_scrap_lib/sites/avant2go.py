import asyncio

import aiohttp
from cache import AsyncTTL
from geojson_pydantic import FeatureCollection, Point, Feature

from slo_scrap_lib.models import RideshareProvider, RidesharePointType, RideshareStation, RideshareVehicle

PROVIDER_ID = "58ee0cc36d818563a9ff46af"


async def get_cars():
    url = 'https://api.avant2go.com/api/cars'
    async with aiohttp.request("GET", url) as resp:
        data = await resp.json()
    return data

get_cars_cached = AsyncTTL(time_to_live=60)(get_cars)


async def get_cars(station_id):
    gj = await get_vehicles_geojson(station_id)
    cars = []
    for feature in gj.features:
        cars.append(RideshareVehicle(
            **feature.properties,
        ))
    return cars


async def get_car_types():
    url = 'https://api.avant2go.com/api/carModels'
    async with aiohttp.request("GET", url) as resp:
        data = await resp.json()
    return {o['_id']: o for o in data}

get_car_types_cached = AsyncTTL(time_to_live=60*60*5)(get_car_types)


async def get_stations():
    url = "https://api.avant2go.com/api/locations"
    params = {
        "limit": 1000,
        "offset": 0,
        "reservable": "true",
        "providerId": PROVIDER_ID
    }

    async with aiohttp.request("GET", url, params=params) as resp:
        data = await resp.json()

    features = []
    for location in data['results']:
        point = Point(coordinates=(location['geoLocation']['lng'], location['geoLocation']['lat']))
        feat = Feature(
            id=location['_id'],
            geometry=point,
            properties=RideshareStation(
                title=location['name'],
                id=location['_id'],
                type=RidesharePointType.CAR_STATION,
                provider=RideshareProvider.AVANT2GO,
                capacity=location['reservableCars'] + location['freeParkingPlaces'],
                capacity_free=location['freeParkingPlaces'],
                vehicles_available=location['reservableCars'],
                image_url=location['mainImageResource']['href'],
            ).dict()
        )
        features.append(feat)

    return FeatureCollection(features=features)


async def get_vehicles_geojson(station_id=None):
    data = await get_cars_cached()
    models = await get_car_types_cached()

    cars = []
    for item in data:
        if station_id and item['locationID'] != station_id:
            continue
        model = models.get(item['carModelID'])
        car = RideshareVehicle(
            system=RideshareProvider.AVANT2GO,
            provider=RideshareProvider.AVANT2GO,
            id=item['_id'],
            license_plate=item['plateNumber'],
            vin=item['vin'],
            range_estimate=item['rangeOffsetFrom'],
            charge_level=item['batteryChargeLevel'],
            price_per_km=item['pricing']['pricePerKM'],
            price_per_min=item['pricing']['pricePerDayMin'],
            price_minimum=item['pricing']['minRentPrice'],
            station_id=item['locationID'],
        )
        if model:
            car.people_capacity = model['seats']
            car.image_url = model['mainImageResource']['href']
            car.model = model['manufacturer'] + ' ' + model['name']
        cars.append(Feature(
            id=item['_id'],
            geometry=Point(coordinates=(item['geoLocation']['lng'], item['geoLocation']['lat'])),
            properties=car.dict()
        ))

    return FeatureCollection(features=cars)


if __name__ == '__main__':
    ft = asyncio.get_event_loop().run_until_complete(get_stations())
    print(ft)

