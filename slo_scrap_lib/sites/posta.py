import dataclasses

import aiohttp

from slo_scrap_api import config_secret


@dataclasses.dataclass(unsafe_hash=True, slots=True, frozen=True)
class PoštnaŠtevilka:
    stevilka: str
    naziv: str

class MojaPosta():
    session: aiohttp.ClientSession

    def __init__(self, username, password):
        self.username = username
        self.password = password

    async def init(self):
        self.session = aiohttp.ClientSession()

    async def login(self):
        data = await self.session.post('https://moja.posta.si/api/auth/login', json={
            "email": self.username,
            "password": self.password
        }).json()

        # set JWT token
        self.session.headers.update({
            'Authorization': 'Bearer ' + data.get('accessToken')
        })

    async def request(self, method, url, **kwargs):
        resp = await self.session.request(method, url, **kwargs)

        if resp.status == 401:
            await self.login()

        resp = await self.session.request(method, url, **kwargs)
        resp.raise_for_status()

        return resp

    async def get_houses_by_postal_code(self, postal_code):
        data = await self.request('POST', 'https://moja.posta.si/api/misc/getHousesByPostalCode', json={
            'postalCode': postal_code
        })

        return await data.json()

    async def get_postal_codes(self):
        data = await self.request('GET', 'https://moja.posta.si/api/misc/getPostalCodes')

        ls = await data.json()
        return [PoštnaŠtevilka(stevilka=d['PostaId'], naziv=d['NazivPosteKratki']) for d in ls]

    async def get_parcel_details(self, tracking_code):
        data = await self.request('POST', 'https://moja.posta.si/api/misc/trackParcel', json={
            'trackingNumber': tracking_code
        })

        return await data.json()


if __name__ == '__main__':
    async def main():
        moja_posta = MojaPosta(config_secret.MOJA_POSTA.username, config_secret.MOJA_POSTA.password)
        await moja_posta.login()
        print(await moja_posta.get_parcel_details('tracking_code'))

    import asyncio
    asyncio.run(main())