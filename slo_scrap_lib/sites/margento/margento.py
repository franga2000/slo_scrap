import asyncio
import json
import os
import secrets

import aiohttp

from base64 import b64decode, b64encode
from pathlib import Path

from Cryptodome.Cipher import AES, PKCS1_OAEP
from Cryptodome.Hash import SHA1
from Cryptodome.PublicKey import RSA
from Cryptodome.Signature import pss

token_lock = asyncio.Lock()


class MargentoException(Exception):
	pass


class MargentoClient:
	"""The "spicy" MargentoClient (with client certs)"""
	DATA_DIR = Path('data/margento')
	SERVER_PUBKEY_FILE = Path('arriva_cert.pem')
	TOKEN_FILE: Path
	AES_PART_FILE: Path
	MY_PRIVKEY_FILE: Path
	server_pubkey = None
	IV = None
	API_BASE: str

	token: str

	def __init__(self, auth_endpoint=None):
		self.TOKEN_FILE = self.DATA_DIR / 'token.json'
		self.AES_PART_FILE = self.DATA_DIR / 'aes_part.bin'
		self.MY_PRIVKEY_FILE = self.DATA_DIR / 'my_privkey.der'
		
		if auth_endpoint:
			self.AUTH_ENDPOINT = auth_endpoint
		os.makedirs(self.DATA_DIR, exist_ok=True)
		# self.load_token()

	def _pad(self, s):
		return s + ((AES.block_size - len(s) % AES.block_size) * chr(AES.block_size - len(s) % AES.block_size)).encode("utf-8")

	def _strip(self, c):
		# Horrible universal stripper - handles zeros and PKCS5/7
		return c.rstrip("".join([chr(i) for i in range(0, 16 + 1)]).encode("utf-8"))

	async def init(self):
		async with token_lock:
			if not self.TOKEN_FILE.is_file():
				resp = await self.get_token()
			else:
				with self.TOKEN_FILE.open("r") as f:
					resp = json.load(f)
			self.token = resp['token']
			self.aes_key = b64decode(resp['__key_plaintext'])
	
	async def get_token(self):
		self.init_rsa()

		req = {    
			"app_version": await self.APP_VERSION(),
			"device_description": "Android 29",
			"device_serial": "2aca2d216102bf11",    
			"key": self.getKeyFieldValue().decode('utf-8'),    
			"platform_id": 1,    
			"public_key": self.my_pubkey.export_key('PEM').replace(b"\n", b"")[26+32:-24].decode("utf-8"),
		}

		resp = await self.auth_request(self.AUTH_ENDPOINT, req)
		# -2: reauth required, -12: customer error, -11: already paired
		assert resp.get('ResultCode', 0) == 0, f"Error {resp['ResultCode']}: {resp['ResultDescription']}"
		resp['__key_plaintext'] = b64encode(self.RSADecrypt(b64decode(resp['key']), self.my_privkey)).decode("utf-8")
		with self.TOKEN_FILE.open("w") as f:
			json.dump(resp, f)
		return resp

	async def APP_VERSION(self):
		return "1.0.8"

	def RSAEncrypt(self, data, key):
		cipher = PKCS1_OAEP.new(key=key, hashAlgo=SHA1, mgfunc=lambda x,y: pss.MGF1(x,y, SHA1))
		ciphertext = cipher.encrypt(data)
		return ciphertext
	
	def RSADecrypt(self, ciphertext, key):
		cipher = PKCS1_OAEP.new(key=key, hashAlgo=SHA1, mgfunc=lambda x,y: pss.MGF1(x,y, SHA1))
		plaintext = cipher.decrypt(ciphertext)
		return plaintext

	def getKeyFieldValue(self):
		if self.AES_PART_FILE.is_file():
			aes_part = self.AES_PART_FILE.read_bytes()
		else:
			aes_part = secrets.token_bytes(16)
			self.AES_PART_FILE.write_bytes(aes_part)
		aes_txt = aes_part.hex().upper()
		
		enc = self.RSAEncrypt(aes_txt.encode("utf-8"), self.server_pubkey)
		return b64encode(enc)

	def init_rsa(self):
		if self.server_pubkey:
			return
		
		self.server_pubkey = RSA.importKey(self.SERVER_PUBKEY_FILE.read_bytes())
		if self.MY_PRIVKEY_FILE.is_file():
			self.my_privkey = RSA.import_key(self.MY_PRIVKEY_FILE.read_bytes())
		else:
			self.my_privkey = RSA.generate(2048)
			self.MY_PRIVKEY_FILE.write_bytes(self.my_privkey.export_key('DER'))
		self.my_pubkey = self.my_privkey.public_key()

	def aes_decrypt(self, ciphertext: bytes) -> bytes:
		iv = self.IV if self.IV else ciphertext[:AES.block_size]
		aes = AES.new(self.aes_key, AES.MODE_CBC, iv)
		strip = 0 if self.IV else AES.block_size
		dec = aes.decrypt(ciphertext[strip:])
		dec = self._strip(dec)
		return dec

	def aes_encrypt(self, plaintext: bytes, iv=None):
		iv = self.IV if self.IV else secrets.token_bytes(AES.block_size)

		aes = AES.new(self.aes_key, AES.MODE_CBC, iv)
		dp = self._pad(plaintext)
		ciphertext = aes.encrypt(dp)

		if self.IV:
			return ciphertext
		else:
			return iv+ciphertext

	async def request(self, url, payload=None):
		if url[0] == '/':
			url = self.API_BASE + url
		if payload is None:
			payload = {"auth_token": self.token}
		if type(payload) != str:
			payload = json.dumps(payload)

		enc = self.aes_encrypt(payload.encode("utf-8")).strip()
		js = {
			"auth_token": self.token,
			"req": b64encode(enc).decode("utf-8"),
		}

		async with aiohttp.ClientSession() as session:
			resp = await session.post(url, json=js)
			if resp.status > 200:
				print(await resp.text())
			resp.raise_for_status()
			resp = await resp.json()
		if not resp['res']:
			return resp['res']
			
		enc = b64decode(resp['res'])
		dec = self.aes_decrypt(enc)

		return json.loads(dec)

	def block_rsa_encrypt(self, payload, key):
		byts = payload.encode("utf-8")
		length = len(byts)
		bs = 2048 // 8 - 42

		ciphertext = b""
		for i in range(length // bs + 1):
			start = bs * i
			remain = length - start
			if remain > bs:
				remain = bs
			block = byts[start:start + remain]
			block_cipher = b64encode(self.RSAEncrypt(block, key))
			ciphertext += block_cipher

		return ciphertext.decode("utf-8")

	def block_rsa_decrypt(self, byts, key):
		length = len(byts)
		bs = 2048 // 8

		plaintext = b""
		for i in range(length // bs):
			start = bs * i
			remain = length - start
			if remain > bs:
				remain = bs
			block = byts[start:start+remain]
			block_cipher = self.RSADecrypt(block, key)
			plaintext += block_cipher

		return plaintext.decode("utf-8")

	async def auth_request(self, url, payload):
		if type(payload) != str:
			payload = json.dumps(payload)
		rsa = self.block_rsa_encrypt(payload, self.server_pubkey).strip()
		js = {
			"auth_token": "",
			"req": rsa,
		}
		async with aiohttp.ClientSession() as session:
			resp = await session.post(url, json=js)
			resp.raise_for_status()
			resp = await resp.json()
		enc = b64decode(resp['res'])
		dec = self.block_rsa_decrypt(enc, self.my_privkey)
		return json.loads(dec)


class MargentoAESClient(MargentoClient):
	"""The "vanilla" MargentoClient (with hard-coded AES keys)"""

	IV = bytes([0] * AES.block_size)
	"""The IV for the AES auth encryption. Leave this as default if generated on the fly. Change it if hard-coded"""

	AUTH_AES_KEY: bytes
	"""The hard-coded AES key for encrypting the auth request. You NEED to override this"""

	async def auth_request(self, url, payload):
		if url[0] == '/':
			url = self.API_BASE + url
		if payload is None:
			payload = {"auth_token": self.token}
		if type(payload) != str:
			payload = json.dumps(payload)

		aes = AES.new(self.AUTH_AES_KEY, AES.MODE_CBC, self.IV)
		dp = self._pad(payload.encode("utf-8"))
		enc_b = aes.encrypt(dp)

		# If IV is different from the default, it means it's hard-coded => we shouldn't include it
		if self.IV == MargentoAESClient.IV:
			enc_b = self.IV + enc_b

		enc = b64encode(enc_b).decode("utf-8")

		js = {
			"req": enc,
			# 'auth_token': None,  # This is sent and ignored by some, but crashes Prehrana
		}

		async with aiohttp.ClientSession() as session:
			resp = await session.post(url, json=js)
			resp.raise_for_status()
			resp = await resp.json()

		resp_enc = b64decode(resp['res'])

		aes = AES.new(self.AUTH_AES_KEY, AES.MODE_CBC, self.IV)

		dec = aes.decrypt(resp_enc)
		plain = self._strip(dec)
		return json.loads(plain)
