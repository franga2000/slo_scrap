import asyncio
from base64 import b64decode
from pathlib import Path

from slo_scrap_lib.sites.margento.margento import MargentoAESClient


class PrehranaClient(MargentoAESClient):
	DATA_DIR = Path('data/margento/prehrana')
	API_BASE = "https://xml.studentska-prehrana.si/SSSPMobile"
	AUTH_ENDPOINT = f"{API_BASE}/authenticate_enc/"
	SERVER_PUBKEY_FILE = Path(__file__).parent / Path('prehrana_cert.pem')

	IV = b64decode("7bjfVPws6d6JPB+9w8py5A==")
	AUTH_AES_KEY = b64decode("ziD3QcDvVpCQiW25Sz3E19srSAkdEw5p3jnxRofdMmE=")

	async def get_lokali(self):
		return (await self.request('/GetLokali_enc/', {}))['Lokali']

	async def get_menus(self, lokal_id):
		r = await self.request('/GetMenus_enc/', {"LokalId":lokal_id})
		if not r:
			return []
		return r['Meniji']


if __name__ == '__main__':
	async def main():
		prehrana = PrehranaClient()
		await prehrana.init()

		lokali = await prehrana.get_lokali()
		print(lokali)
	asyncio.run(main())
