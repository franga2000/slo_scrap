import asyncio
from pathlib import Path


from .margento import MargentoClient


class ArrivaClient(MargentoClient):
    DATA_DIR = Path('data/margento/arriva')
    SERVER_PUBKEY_FILE = Path(__file__).parent / 'arriva_cert.pem'
    API_BASE = "http://ijpp.arriva.si/ArrivaAPI"
    AUTH_ENDPOINT = f"{API_BASE}/authenticate_enc2/"

    async def get_stations(self):
        """List of bus stations"""
        params = {
            "count": 150,
            "data": "",  # search query
            "index": 0,
            "latitude": 45.52778,
            "longitude": 13.57056,
            "type": 0,
        }
        resp = await self.request("/get_stations_enc/", params)
        return resp

    async def get_lines(self):
        """List of bus lines"""
        resp = await self.request("/get_lines_enc/")
        return resp

    async def get_stations(self):
        """List of bus stations"""
        resp = await self.request("/get_stations_enc/")
        return resp

    async def get_buses(self):
        """List of buses with current position"""
        resp = await self.request("/get_bus_locations_enc/")
        return resp

    async def request_otp(self, phone_num: str):
        """
        :param phone_num: Phone number in international form without 00/+. e.g. 40123456
        """
        req = {"language": "EN", "msisdn": phone_num, "auth_token": self.token}
        resp = await self.request("/register_msisdn_enc/", req)
        return resp

    async def confirm_otp(self, otp: str):
        """
        :param otp: The 4-digit code, received by SMS
        """
        req = {"otp": otp, "auth_token": self.token}
        resp = await self.request("/register_confirm_enc/", req)
        return resp


if __name__ == '__main__':
    async def main():
        client = ArrivaClient()
        await client.init()
        stations = await client.get_stations()
        lines = await client.get_lines()
        buses = await client.get_buses()
        print(buses)


    asyncio.run(main())
