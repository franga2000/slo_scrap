import asyncio
import json
from base64 import b64decode, b64encode
from enum import Enum
from pathlib import Path
from typing import Union

from Cryptodome.Cipher import AES

from .margento import MargentoAESClient, MargentoException


class UrbanaError(Enum):
    ERROR_NO_PARKING_CANNOT_EXTEND = -632
    ERROR_NO_PARKING_PAYMENT_USELESS = -631
    ERROR_NO_PARKING_PAYMENT_TO_SOON = -630
    ERROR_NO_PARKING_PAYMENT_INVALID_DATA2 = -621
    ERROR_NO_PARKING_PAYMENT_INVALID_DATA = -619
    ERROR_NO_PARKING_PAYMENT_UNREGISTERED = -562
    ERROR_NO_PARKING_PAYMENT_INVALID_CREDITS = -507
    ERROR_GEAR_BALANCE_TOO_BIG = -104
    ERROR_GEAR_BALANCE_TOO_SMALL = -103
    ERROR_PARKING_TOO_SOON = -27
    ERROR_PARKING_VIRTUAL_ACCOUNT_DOES_NOT_EXIST = -26
    ERROR_LOGIN_APP_NOT_UP_TO_DATE = -25
    ERROR_LOGIN_BLOCK = -24
    ERROR_NO_PARKING_PAYMENT = -21
    ERROR_NO_MONETA_TOPUP_TOO_MUCH = -20
    ERROR_NO_PARKING_ZONES = -19
    ERROR_NO_GARAGE_INFO = -18
    ERROR_NO_STATIONS_ON_ROUTE = -17
    ERROR_NO_STATION_ARRIVALS = -16
    ERROR_NO_BIKE_STATION_INFO = -15
    ERROR_POI_SEARCH_NO_RESULTS = -14
    ERROR_HISTORY_EMPTY = -13
    ERROR_MONETA_TOPUP_ERROR = -12
    ERROR_MONETA_RESEND_OVERUSED = -11
    ERROR_MONETA_CONFIRM_CONDE_INVALID = -10
    ERROR_MONETA_REGISTRATION_NOT_SENT = -9
    ERROR_APP_NOT_REGISTERED = -8
    ERROR_MONETA_ALREADY_REGISTERED = -7
    ERROR_NO_CUSTOMER_FOUND = -6
    ERROR_TEST_DEVICE_DISABLED = -5
    ERROR_NOT_TEST_USR = -4
    ERROR_APP_BLOCKED = -3
    ERROR_INVALID_AUTH = -2
    ERROR_UNKNOWN = -1
    OK = 0
    OK_LOGIN_REGISTER = 1
    OK_LOGIN_MESSAGE = 2


class UrbanaClient(MargentoAESClient):
    DATA_DIR = Path('data/margento/urbana')
    API_BASE = "https://mpsa.mestna.net/UrbanaAPI2/API2/"
    AUTH_ENDPOINT = f"{API_BASE}/authenticate_enc/"
    SERVER_PUBKEY_FILE = Path(__file__).parent / Path('urbana_server.cer')

    IV = bytes.fromhex('EDB8DF54FC2CE9DE893C1FBDC3CA72E4')
    AUTH_AES_KEY = bytes.fromhex('CE20F741C0EF569090896DB94B3DC4D7DB2B48091D130E69DE39F14687DD3261')

    klipkey: bytes  # Some NFC shit, probably useful later

    async def init(self):
        if not self.TOKEN_FILE.is_file():
            r1 = await self.get_token()
            r2 = await self.register()
            resp = r1 | r2
        else:
            with self.TOKEN_FILE.open("r") as f:
                resp = json.load(f)
        self.token = resp['token']
        self.klipkey = b64decode(resp['__key_plaintext'])

    def APP_VERSION(self):
        return "2.26"

    def device_info(self):
        # TODO: GUID generation
        ANDROID_ID = '11893253229555bf'
        return {
            # "deviceDescription": "SM-G920F,Samsung,10!" + ANDROID_ID + ":" + sha512(ANDROID_ID.encode('ascii')).hexdigest(),
            # "deviceDescription": "SM-G920F,Samsung,10!FUCKYOU:796dcc52c776e14bf60df04bd4889079db3fe55f0dc17195f316b8491926a477db02d019c0e734d32ce4b1fa4e2dfd4e90cbee13f51f78b0e83fdc838c3163a4",
            # "guid": "36219630362851848",
            # "guid": "36300314869653001",
            # "appVersion": await self.APP_VERSION(),
            "appVersion": "2.26",
            "deviceDescription": "ONEPLUS A3003,OnePlus,11!21341:5879135dc0ffe714738e06e250d13a3d42b742cee866302f66830b2a324df87a918f2e5c5389eedc72cbe2507a916881d0379352cce1b61dade1c7bf63d0836a",
            "guid": "36098468658529288",
            "other_guid": ""
        }

    async def get_token(self):
        self.init_rsa()

        req = {
            "other_guid": "",
        } | self.device_info()

        resp = await self.auth_request(self.API_BASE + '/login_enc', req)
        self.check_status(resp)

        with self.TOKEN_FILE.open("w") as f:
            json.dump(resp, f)

        return resp

    # This one uses different padding from the rest of the apps. Using PKCS5 gives a 400 error
    def _pad(self, s):
        return s + (b'\0' * (AES.block_size - len(s) % AES.block_size))

    async def request(self, url, payload: Union[str, dict, list] = None):
        if 'token' not in payload:
            payload['token'] = self.token
        return await self.auth_request(url, payload)

    async def register(self):
        self.init_rsa()

        req = {
            "ei": "",
            "password": "",
            "email": "",
            "registerType": 2,
            "publickey": self.my_pubkey.export_key('PEM').replace(b"\n", b"")[26 + 32:-24].decode("utf-8"),
            "si": "N/A",
            "token": self.token
        } | self.device_info()

        resp = await self.auth_request(self.API_BASE + '/register_ss_enc/', req)
        self.check_status(resp)

        resp['__key_plaintext'] = b64encode(self.RSADecrypt(b64decode(resp['ss']), self.my_privkey)).decode("utf-8")
        with self.TOKEN_FILE.open('r') as f:
            js = json.load(f)
        with self.TOKEN_FILE.open("w") as f:
            json.dump(js | resp, f)

        return resp

    def check_status(self, r):
        s = UrbanaError(r['status'])
        if s not in (
                UrbanaError.OK,
                UrbanaError.OK_LOGIN_MESSAGE,
                UrbanaError.OK_LOGIN_REGISTER
        ):
            raise MargentoException(f"{s.name} ({s.value})")

    ######################
    #   USER FUNCTIONS   #
    ######################

    async def moneta_register_number(self, phone_number: str):
        r = await self.request('/moneta_register_enc', {"number": phone_number})
        self.check_status(r)

    async def moneta_submit_otp(self, code: str):
        r = await self.request('/moneta_register_confirm_enc', {"code": code})
        s = UrbanaError(r['status'])
        if s == UrbanaError.ERROR_MONETA_CONFIRM_CONDE_INVALID:
            return False
        if r['status'] == UrbanaError.OK:
            return True
        self.check_status(r)

    async def moneta_topup(self, amount_cents: str):
        raise NotImplementedError("TODO")
        p = {
            "amount": amount_cents,
            "currentAmount": 0,
            "pin": "",
            "ss": "---TODO---",
            "transactionId": 1658614876,
        }
        r = await self.request('/moneta_topup_ss_enc', p)
        self.check_status(r)
        # TODO: decode SS
        return r

    async def get_history(self, page=0, page_size=15):
        r = await self.request('/history_enc/', {"count": page_size, "offset": page * page_size})
        if r['status'] == UrbanaError.ERROR_HISTORY_EMPTY:  # ERROR_HISTORY_EMPTY
            return []
        self.check_status(r)
        return r['lines']

    async def set_transfer_email(self, email: str):
        r = await self.request('/transfer_mail_enc/', {"email": email})
        self.check_status(r)


if __name__ == '__main__':
    async def main():
        client = UrbanaClient()
        await client.init()

        # for i in range(9999):
        #     code = str(i).zfill(4)
        #     success = await client.moneta_submit_otp(code)
        #     print(i, success)
        #     if success:
        #         break
        #     await asyncio.sleep(.3)

        print(await client.get_history())

        pass


    asyncio.run(main())
