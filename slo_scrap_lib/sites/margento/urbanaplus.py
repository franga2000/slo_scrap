from enum import IntEnum

import aiohttp

from slo_scrap_lib.models import oJPP_Kiosk


class UrbanaPOIType(IntEnum):
    OTHER = 0
    BUS = 1
    PARKING = 2
    URBANOMAT = 3
    BICIKELJ = 4
    URBANA_POS = 5
    CABLE_LIFT = 6
    CLOSED_PARKING = 7
    PARK_AND_RIDE = 8
    ROAD_BARRIER = 9
    LANDMARK = 10
    LIBRARY = 11
    CITY_E_BUS = 12
    HOME = 13
    WORK = 14
    LOCATION = 15
    BUS_ORDER_RIDE = 16
    ORDER_RIDE = 17
    UPARK = 18
    BUS_ROUTE = 19

    X_UNKNOWN_1 = 25


class UrbanaPlusClient:
    API_BASE = 'https://mobilews.margento.com/'

    async def request(self, method, url, **kwargs):
        if url[0] == '/':
            url = self.API_BASE + url

        async with aiohttp.ClientSession() as session:

            resp = await session.request(method, url, **kwargs)
            if resp.status > 200:
                print(await resp.text())
            resp.raise_for_status()
            resp = await resp.json()
        return resp

    async def get_pois(self):
        return (await self.request('POST', '/pois', json={'last_date': '1970-01-01T00:00:00.000Z'}))['pois']

    async def get_kiosks(self):
        pois = await self.get_pois()
        return [oJPP_Kiosk(
            id=str(poi['id']),
            name=poi['name'],
            lat=poi['latitude'],
            lng=poi['longitude'],
        ) for poi in pois if poi['type'] == UrbanaPOIType.URBANOMAT.value]
