import asyncio
from pathlib import Path

from slo_scrap_lib.sites.margento.margento import MargentoAESClient


class MarpromClient(MargentoAESClient):
    DATA_DIR = Path('data/margento/marprom')
    API_BASE = "http://ws.marprom.si/MarpromAPI"
    AUTH_ENDPOINT = f"{API_BASE}/authenticate_enc/"
    SERVER_PUBKEY_FILE = Path(__file__).parent / Path('marprom_cert.pem')

    AUTH_AES_KEY = bytes.fromhex('0AB379C88A6143806833EECC09C2D50E15F8920CA23D2E781BDE5BF7B3B23327')

    async def APP_VERSION(self):
        return "1.1.4"

    async def get_products(self):
        resp = await self.request('/GetAvailableProducts_enc', '{}')
        return resp

    async def get_card_products(self, cuid: int):
        p = {"CUID": cuid}
        resp = await self.request('/GetCardProducts_enc', p)
        return resp


if __name__ == '__main__':
    async def main():
        client = MarpromClient()
        await client.init()

        p = await client.get_card_products(36132327111403524)
        print(p)


    asyncio.run(main())
