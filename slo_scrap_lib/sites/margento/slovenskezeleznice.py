import asyncio
import time
from pathlib import Path

from slo_scrap_lib.sites.margento.margento import MargentoAESClient


class SZClient(MargentoAESClient):
    DATA_DIR = Path('data/margento/slovenskezeleznice')
    API_BASE = "https://eshop.sz.si/SZMobile"
    AUTH_ENDPOINT = f"{API_BASE}/authenticate_enc/"
    SERVER_PUBKEY_FILE = Path(__file__).parent / Path('sz_cert.pem')

    AUTH_AES_KEY = bytes.fromhex('0AB379C88A6143806833EECC09C2D50E15F8920CA23D2E781BDE5BF7B3B23327')

    async def get_ovire(self):
        p = {"language": 0}
        resp = await self.request('/GetOvire_enc', p)
        return resp

    async def get_routes(self, start_station: int, end_station: int):
        p = {"date": f"/Date({ int(time.time()) }+0100)/", "endStationId": end_station, "isReverse": False, "startStationId": start_station}
        resp = await self.request('/GetRoutesSz_enc', p)
        return resp

    async def get_card_info(self, cuid):
        p = f'{{"CUID":"{cuid}"}}'
        resp = await self.request('/GetCard_enc', p)
        return resp


# List of endpoints:
#  - authenticate_enc/
#  - GetCard_enc/
#  - GetCatalogs/
#  - get_messages_enc/
#  - GetOvire_enc/
#  - GetProducts_enc/
#  - GetReceipt_enc/
#  - GetRoutesSz_enc/
#  - GetStations_enc/
#  - GetSzCardProducts_enc/
#  - SetReceipt_enc/
#  - SetReceiptSz_enc/
#  - update_message_enc/


if __name__ == '__main__':
    async def main():
        client = SZClient()
        await client.init()
        resp = await client.request('/GetOvire_enc')
        print(resp)
        pass

    asyncio.run(main())
