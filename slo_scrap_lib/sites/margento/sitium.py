import asyncio
from pathlib import Path


from slo_scrap_lib.sites.margento.margento import MargentoClient


class SitiumClient(MargentoClient):
    DATA_DIR = Path('data/margento/sitium')
    SERVER_PUBKEY_FILE = Path('sitium_cert.pem')
    API_BASE = "https://mobile-sitium.telekom.si"
    AUTH_ENDPOINT = f"{API_BASE}/authenticate_enc2/"

    async def get_cities(self):
        return await self.request('/get_cities_enc/', {})

    async def get_parking_zones(self):
        return await self.request('/parking_zones_enc/', {})


if __name__ == '__main__':
    async def main():
        client = SitiumClient()
        await client.init()
        buses = await client.request('/parking_zones_enc/', {})
        print(buses)


    asyncio.run(main())
