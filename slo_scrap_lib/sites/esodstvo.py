import logging
import pickle
import re
from enum import Enum

import requests
from captcha_solver import CaptchaSolver


class CaptchaException(Exception):
    pass


class TipNepremicnine(str, Enum):
    PARCELA = 1
    STAVBA = 2
    DEL_STAVBE = 3
    STAVBNA_PRAVICA = 4
    VISEČA_STAVBA = 5
    NERAZDELJENI_DELI = 6


class eVlozisce():
    FORM_RE = re.compile('name=\"(?P<name>.+)\" value=\"(?P<value>.*)\" />')
    logger = logging.getLogger("eVlozisce")

    def __init__(self, captcha_solver=None, session_file=None):
        self.captcha_solver = captcha_solver or CaptchaSolver("browser")
        self.url = "https://evlozisce.sodisce.si"
        self._sess = requests.Session()
        self._sess.headers["User-Agent"] = "Mozilla/5.0 (X11; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0"
        self.session_file = session_file
        if self.session_file:
            self.try_load_session()

    def login(self, username, password):
        # Preamble
        self._sess.get(self.url + "/esodstvo/prijava.html?type=navaden")
        self._sess.post(self.url + "/esodstvo/navaden-prijava.ajax")

        resp = self._sess.post(self.url + "/esodstvo/j_vs_security_check", data={
            "j_username": username,
            "j_password": password
        })

        self.try_save_session()

        return resp

    def role_eopravila_reg(self):
        return self.request_with_captcha("GET", self.url + "/esodstvo/r/o/l/e/201/go.html")

    def izberi_ezk(self):
        r1 = self.request_with_captcha("GET", self.url + "/esodstvo/izberiAplikacijo-970066599.html")
        fields = [m.groupdict() for m in self.FORM_RE.finditer(r1.text)]
        data = dict((f["name"], f["value"]) for f in fields)
        # print(data, fields)
        r2 = self.request("POST", self.url + "/evlozisce/j_vs_security_check_token", data=data)

        return r2

    def ezk_izpis(self, tip_nepremicnine: TipNepremicnine, katastrska_občina, številka_parcele=None, številka_stavbe=None, številka_dela_stavbe=None, številka_VS=None):
        # Preamble
        resp = self.request_with_captcha("GET", self.url + "/evlozisce/javni_izpisi/list.html")

        data = {
            "nacinList": "IDZNAK",
            "idNep": "",
            "tipNepList.nobind": tip_nepremicnine.value,
            "tipNep": tip_nepremicnine.value,
            "nacinIskanja": "IDZNAK",
            "_evendId_pdf": ""
        }

        if katastrska_občina:
            data["idZnakNep.katastrskaObcina.idsrcsifrant"] = katastrska_občina
        if številka_parcele:
            data["idZnakNep.parcelnaStevilkaNovo"] = številka_parcele
        if številka_stavbe:
            data["idZnakNep.stevilkaStavbe"] = številka_stavbe
        if številka_dela_stavbe:
            data["idZnakNep.stPosameznegaDela"] = številka_dela_stavbe
        if številka_VS:
            data["idZnakNep.stevilkaVS"] = številka_VS

        resp = self.request_with_captcha("POST", self.url + "/evlozisce/javni_izpisi/03-001.html", data=data)
        assert "Content-Disposition" in resp.headers, "Response doesn't have an attachment!"
        return resp

    def request(self, *args, **kwargs):
        resp = self._sess.request(*args, **kwargs)
        if b"/evlozisce/captchaImg" in resp.content:
            raise CaptchaException()
        return resp

    def request_with_captcha(self, *args, max_tries=3, **kwargs):
        if max_tries < 1:
            return None
        for _ in range(max_tries):
            try:
                return self.request(*args, **kwargs)
            except CaptchaException:
                img = self._sess.get(self.url + "/evlozisce/captchaImg")
                self.logger.info("Waiting on captcha...")
                solution = self.captcha_solver.solve_captcha(img.content)
                resp = self.request_with_captcha("GET", self.url + f"/evlozisce/javni_izpisi/list.html?submitedCaptchaAnswer={solution}&_eventId_btn.send=Pošlji", max_tries=max_tries - 1)

    def try_load_session(self):
        try:
            with open(self.session_file, "rb") as f:
                cookiejar = pickle.load(f)
        except FileNotFoundError:
            pass
        else:
            self._sess.cookies.update(cookiejar)
            self.logger.info("Loaded session")

    def try_save_session(self):
        with open(self.session_file, "wb") as f:
            pickle.dump(self._sess.cookies, f)

