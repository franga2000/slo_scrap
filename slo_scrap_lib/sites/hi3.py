import logging
from dataclasses import dataclass
from typing import List, Dict, Union
from urllib.parse import urlencode

from pydantic import BaseModel
from requests import Request

from slo_scrap_lib.fetcher import AutoProxyFetcher


class Fetcher(AutoProxyFetcher):
    pass

FETCHER = Fetcher()


# @dataclass
class EurotaxResult(BaseModel):
    eurotax: int
    title: str
    kw: int
    door: int
    begin: int
    end: int
    impbegin: str
    impend: str
    price: float
    ccm: int
    makename: str
    typname: str
    typdoor: int
    typseat: int
    id: int
    label: str
    value: str

def _try_dataclasses(DataClass, lst: List[Dict]):
    for dic in lst:
        try:
            yield DataClass(**dic)
        except:
            if not dic["id"] == "-1":
                logging.exception("Exception while applying DataClass")

def eurotax_search(letnik: int, naziv: str) -> List[EurotaxResult]:
    url = "https://www.hi3.si/zm/helper/etaxautocomplete"

    naziv = naziv.lower()
    for sub in ( ('ceed','cee\'d'), ('citroen','citroën'), ):
        naziv = naziv.replace(sub[0], sub[1])

    params = dict(letnik=letnik, naziv=naziv)
    resp = FETCHER.ensure_request(Request("GET", url, params=params))
    return list(_try_dataclasses(EurotaxResult, resp.json()))


#@dataclass
class Zavarovanje(BaseModel):
    premijaNetoBrezPopustov: float
    premijaNeto: float
    dpzpStopnja: float
    nematerialnaZV: float
    materialnaZV: float
    dpzp: float
    premijaBruto: float
    popDopA: List

@dataclass
class Zavarovanja:
    ao: Zavarovanje
    aoPlus: Zavarovanje
    aoAxa: Zavarovanje
    nezgPotnikov: Zavarovanje
    zelenaKarta: Zavarovanje


def url_flatten(d):
    return dict(kv_translation(d, "", ""))


def kv_translation(d, line, final_str):
    for key in d:
        key_str = key if not line else "[{}]".format(key)
        if type(d[key]) is dict:
            yield from kv_translation(d[key], line + key_str, final_str)
        elif type(d[key]) is list:
            for val in d[key]:
                yield "{}{}{}".format(final_str, line, key_str), val
        else:
            yield "{}{}{}".format(final_str, line, key_str), d[key]


@dataclass
class BackendException(Exception):
    data: Union[Dict, List]


def zavarovanja(eurotax, letnik, naziv):
    url = "https://www.hi3.si/avtomobili/zavarovanje/zavarovanjeformret"
    data = {
        "korak1": {
            "eurotaxCode": eurotax,
            "letnik": letnik,
            "naziv": naziv.replace(" ", "+"),
            "potreba": "ne",
            "pravnoObvestilo": 1,
       #     "pravnoObvestilo": [0, 1],
       #     "enkratnoPlacilo": [0, 1]
            "enkratnoPlacilo": 1,
        },
        "korak2": {
            "spol": "z",
            "datumZav": "05.10.2021",
        },
        "korak3": {
            "namen": 0,
            "ipid": 0,
            "kartica": "WSPAY",
            "initwspos": "true",
        },
        "validate_type": "partial",
        "validate_subform": "korak1",
        "validate_recalc": "true",
    }
    encoded = url_flatten(data)
    encoded = urlencode(encoded, safe="()+")
    resp = FETCHER.ensure_request(Request("POST", url, data=encoded, headers={'content-type': 'application/x-www-form-urlencoded'}))

    rj = resp.json()
    if "izracun" not in rj:
        raise BackendException(rj)
    dic: Dict = rj["izracun"]
    keys = set(dic.keys()) - {"skupaj", "dodatno"}

    return Zavarovanja(**{
        key: Zavarovanje(**dic[key])
        for key in keys
    })


if __name__ == '__main__':
    year = 2011
    ets = eurotax_search(year, "308 1,6 e-HDi Active")
    print(ets)
    et = ets[0]
    zs = zavarovanja(et.id, year, et.title)
    print(zs)
