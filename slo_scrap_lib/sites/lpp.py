import datetime
import datetime
import re
from typing import Dict

import pytz
from bs4 import BeautifulSoup
from requests import Request

from slo_scrap_lib.fetcher import AutoProxyFetcher
from slo_scrap_lib.html_util import textify, select_to_tuples
from slo_scrap_lib.models import Stop, LineInfo, TransitInfo
from slo_scrap_lib.sites import urbana
from slo_scrap_lib.sites.urbana import UrbanaPOI


class Fetcher(AutoProxyFetcher):
    pass

FETCHER = Fetcher()

STOPS_RE = re.compile(r'var marker = new google.maps.Marker.+?position.+?LatLng\(([0-9.]+), ([0-9.]+).+?title: \"(.+?)\".+?location\.href.+?stop=(.+?)&.+?', flags=re.S)
PATHS_RE = re.compile(r'PathStyle = .+?],', flags=re.S)
COORDS_RE = re.compile(r'LatLng\(([0-9.]+?), ([0-9.]+?)\),', flags=re.S)

#
#
# https://data.lpp.si/api/station/arrival?station-code=300015
#
#

def info(include_coords=False) -> TransitInfo:
    url = "https://www.lpp.si/sites/default/files/lpp_vozniredi/iskalnik/"
    resp = FETCHER.ensure_request(Request("GET", url))
    html = BeautifulSoup(resp.text, "html.parser")
    _stops = select_to_tuples(html.select_one("#stop"))

    stops = {}
    for num, name in _stops:
        if num:
            name = name.replace(" (" + num + ")", "")
            stops[num] = Stop(id=int(num), name=name, ids={'lpp_human': num})

    _lines = select_to_tuples(html.select_one("#line"))
    lines = {}
    for num, name in _lines:
        if num:
            lines[num] = LineInfo(id=int(num), name=textify(BeautifulSoup(name, "html.parser")))

    if include_coords:
        pois: Dict[str, UrbanaPOI] = {poi.nameNumber: poi for poi in urbana.get_pois()}
        for stop in stops.values():
            stop.lat = pois[str(stop.id)].latitude
            stop.lng = pois[str(stop.id)].longitude

    return TransitInfo(
        stops=stops,
        lines=lines,
    )


def odhodi(linija_id: int, postaja_id: str, dir: int):
    url = "https://www.lpp.si/sites/default/files/lpp_vozniredi/iskalnik/js/departures.php"
    data = {
        'lineId': linija_id,
        'stopId': int(postaja_id),
        'dir': int(dir),
    }
    resp = FETCHER.ensure_request(Request("POST", url, data=data))
    html = BeautifulSoup(resp.text, "html.parser")
    service_periods = html.select(".departures > ul > li")[1:]

    CURRENT_TZ = pytz.timezone('Europe/Ljubljana')
    sps = []
    for container in service_periods:
        times = [textify(el).split(":") for el in container.select('time')]
        times = [datetime.time(hour=int(tim[0]), minute=int(tim[1]), tzinfo=CURRENT_TZ) for tim in times]
        sps.append(times)
    return sps


def stop_info(human_stop_id):
    url = "https://www.lpp.si/sites/default/files/lpp_vozniredi/iskalnik/"
    params = {
        'stop': human_stop_id,
    }
    resp = FETCHER.ensure_request(Request("GET", url, params=params))
    html = BeautifulSoup(resp.text, "html.parser")

    _lines = html.select(".lineWrapper .lineFiles")

    real_stops = {}
    for _line in _lines:
        if not (btn := _line.select_one('.btn.times')):
            continue
        stop_internal_id = btn.attrs['onclick'].rsplit(", ", maxsplit=1)[-1][:-1]
        if stop_internal_id not in real_stops:
            dir = 1 if 'dir1' in _line.attrs['class'] else 2
            human_stop_id = textify(html.select_one('#stop-header-title span')) + "-" + str(dir)
            real_stops[stop_internal_id] = Stop(
                id=stop_internal_id,
                lines=[],
                name=textify(html.select_one('#stop-header-title')),
                ids={'lpp_human': human_stop_id, 'lpp_internal': stop_internal_id},
                dir=dir
            )

        num, heading = _line.select('h3 > strong')
        line_id = int(_line.parent.parent.attrs['id'].replace('line', ''))

        line = LineInfo(
            id=line_id,
            heading=textify(heading),
            name=textify(num),
            stops=[],
        )
        for _stop in _line.parent.select('.line-dir-stop .stop'):
            human_id = _stop.attrs['href'].split("&")[0].split("=")[1]
            line.stops.append(Stop(
                name=textify(_stop),
                id=human_id,
                ids={'lpp_human': human_id},
            ))
        real_stops[stop_internal_id].lines.append(line)

    return list(real_stops.values())


def linija_info(linija_id):
    url = "https://www.lpp.si/sites/default/files/lpp_vozniredi/iskalnik/"
    params = {
        'line': linija_id,
    }
    resp = FETCHER.ensure_request(Request("GET", url, params=params))
    html = BeautifulSoup(resp.text, "html.parser")

    smer_1, smer_2 = html.select(".lineWrapper > .col-md-6")

    stops = []

    return LineInfo(
        stops=stops,
    )


if __name__ == '__main__':
    stops = stop_info('600012')
    print(stops)
    dept = odhodi(linija_id=stops[0].lines[0].id, postaja_id=stops[0].id, dir=stops[0].dir)
    print(dept)
    # stops = stop_info("103111")
    # print(stops)
    pass






