from datetime import datetime

import aiohttp

from slo_scrap_lib.models import oJPP_Vožnja
from slo_scrap_lib.util import hour_on_date


# BASE_URL = "http://127.0.0.1:8080"
BASE_URL = "http://chili.derp.si:8081"


async def seznam_postaj():
    url = f"{BASE_URL}/stations"
    async with aiohttp.request("GET", url) as resp:
        return await resp.json()


MARPROM_OPOZORILA = ['Podatki AP Maribor so nezanesljivi, preverite pri PREVOZNIKU!']


async def vozni_red(vstop_id: str, izstop_id: str, iso_datum: str):
    datum = datetime.strptime(iso_datum, "%Y-%m-%d")

    url = f"{BASE_URL}/times/{vstop_id}/{izstop_id}/{iso_datum}"

    async with aiohttp.request("GET", url) as resp:
        vožnje = []
        for vožnja in await resp.json():
            v = oJPP_Vožnja(
                prihod=hour_on_date(datum, vožnja["prihod"]),
                odhod=hour_on_date(datum, vožnja["odhod"]),
                razdalja=vožnja["razdalja"],
                prevoznik=vožnja["prevoznik"],
                naziv=vožnja["naziv"],
                opozorila=MARPROM_OPOZORILA,
            )
            vožnje.append(v)

    return vožnje
