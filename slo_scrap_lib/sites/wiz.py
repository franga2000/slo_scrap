import aiohttp
from aiohttp import ClientSession
from bs4 import BeautifulSoup

from slo_scrap_lib.html_util import extract_form_fields
from slo_scrap_api import config_secret


async def car_data(plate="", vin=""):
    plate = plate.upper().replace(" ", "").replace("-", "")

    async with aiohttp.ClientSession() as session:
        session: ClientSession
        session.headers.update({
            "User-Agent": 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36'
        })

        # Get cookie
        async with session.get('https://avto.wiz.si/') as resp1:
            text1 = await resp1.text()
            resp1.raise_for_status()

        # Send plate
        async with session.post('https://avto.wiz.si/skleni/-/avto/zacetekA-dopolni', data={
            'stepZeroForm.registrationNumberPrefix': plate[:2],
            'stepZeroForm.registrationNumber': plate[2:],
        }) as resp2:
            text2 = await resp2.text()
            resp2.raise_for_status()
    text2 = text2.replace('stepOneAForm.', '')
    html = BeautifulSoup(text2, 'html.parser')

    data = extract_form_fields(html, select_contents=True)

    return data


if __name__ == '__main__':
    import asyncio
    loop = asyncio.get_event_loop()
    data = loop.run_until_complete(car_data('MB 02-LIT'))
    print(data)

