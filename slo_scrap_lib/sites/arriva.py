import asyncio
import datetime
import json

import aiohttp
from cache import AsyncTTL

from slo_scrap_lib.models import oJPP_Vožnja
from slo_scrap_lib.util import hour_on_date


@AsyncTTL(time_to_live=60)
async def get_api_data():
    async with aiohttp.request("GET", 'https://arriva.si/pravno-obvestilo/') as resp:
        async for line in resp.content:
            if line.startswith(b'var apiData'):
                break
    return json.loads(line[14:-2])


async def seznam_postaj():
    api_data = await get_api_data()
    url = f'https://prometws.alpetour.si/WS_ArrivaSLO_TimeTable_DepartureStations.aspx?cTimeStamp={api_data["datetime"]}&cToken={api_data["cTimeStamp"]}&json=1'
    async with aiohttp.request('GET', url) as resp:
        resp = json.loads(await resp.content.read())

    postaje = {
        p['JPOS_IJPP']: p['POS_NAZ']
        for p in resp[0]['DepartureStations']
    }

    return postaje


async def vozni_red(vstop_id: str, izstop_id: str, iso_datum: str):
    dt = datetime.datetime.strptime(iso_datum, '%Y-%m-%d')
    api_data = await get_api_data()
    url = f'https://prometws.alpetour.si/WS_ArrivaSLO_TimeTable_TimeTableDepartures.aspx?cTimeStamp={api_data["datetime"]}&cToken={api_data["cTimeStamp"]}&JPOS_IJPPZ={vstop_id}&JPOS_IJPPK={izstop_id}&VZVK_DAT={iso_datum}&json=1'
    async with aiohttp.request('GET', url) as resp:
        resp = json.loads(await resp.content.read())

    vožnje = []

    for p in resp[0]['Departures']:
        v = oJPP_Vožnja(
            odhod=hour_on_date(dt, p['ROD_IODH']),
            prihod=hour_on_date(dt, p['ROD_IPRI']),
            peron=p['ROD_PER'],
            razdalja=p['ROD_KM'],
            opozorila=[p['ROD_OPO']] if p['ROD_OPO'] else [],
            cena=p['VZCL_CEN'],
            prevoznik=p['RPR_NAZ'],
        )
        vožnje.append(v)
    return vožnje


if __name__ == '__main__':
    async def main():
        ft = await seznam_postaj()
        #print(ft)
        vz = await vozni_red(139129, 138368, datetime.date.today().isoformat()[0:10])
        print(vz)

    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
