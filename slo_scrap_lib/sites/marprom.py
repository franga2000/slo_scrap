import asyncio
import datetime
import re
from collections import defaultdict

import aiohttp
import dateutil.parser
from bs4 import BeautifulSoup

from slo_scrap_lib.html_util import table_to_dict_list, textify
from slo_scrap_lib.models import Stop, LineInfo
from slo_scrap_lib.util import hour_on_date

STOPS_RE = re.compile(r'var marker = new google.maps.Marker.+?position.+?LatLng\(([0-9.]+), ([0-9.]+).+?title: \"(.+?)\".+?location\.href.+?stop=(.+?)&.+?', flags=re.S)
STOP_NUMBERS_RE = re.compile(r'<tr onclick=".+?/\?stop=(\d+?)&.+?<b class="paddingTd">(\d+?)<', flags=re.S)
PATHS_RE = re.compile(r'PathStyle = .+?],', flags=re.S)
COORDS_RE = re.compile(r'LatLng\(([0-9.]+?), ([0-9.]+?)\),', flags=re.S)


async def seznam_postaj():
    url = "https://vozniredi.marprom.si/?all=all"
    async with aiohttp.request("GET", url) as resp:
        stops = _get_stops(await resp.text())
    return {stop.id: stop for stop in stops}


def _get_stops(text):
    number_map = {name.strip(): number for number, name in STOP_NUMBERS_RE.findall(text)}
    stops = []
    for lat, lng, name, id in STOPS_RE.findall(text):
        stops.append(Stop(
            id=int(id),
            lat=float(lat),
            lng=float(lng),
            name=name.strip(),
            ids=dict(st_postaje=number_map.get(id)),
        ))
    return stops


async def odhodi(vstop_id: str, iso_datum: str):
    url = f"https://vozniredi.marprom.si/"
    params = {
        "stop": vstop_id,
        "datum": iso_datum,
    }
    async with aiohttp.request("GET", url, params=params) as resp:
        html = BeautifulSoup(await resp.text(), "html.parser")
    modal = html.select_one("#ModalBodyStopInfo")

    dat = dateutil.parser.isoparse(iso_datum)

    odhodi_lst = defaultdict(dict)
    tables = modal.select("table.table-bordered tbody")[2:]
    btns = modal.select(".modal-btn-route-position")

    for table, btn in zip(tables, btns):
        line = textify(btn)
        table_data = table_to_dict_list(table)
        for smer in table_data:
            naziv = smer['Smer']
            times = smer['Predvideni odhodi'].split(" ")
            datetimes = [hour_on_date(dat, odhod_str) for odhod_str in times]
            odhodi_lst[line][naziv] = datetimes

    return dict(odhodi_lst)


async def linija_info(linija_id, iso_datum=None):
    iso_datum = iso_datum or datetime.date.today().isoformat()
    url = "https://vozniredi.marprom.si/sl-SI/Home/Index/"
    data = {
        "route": linija_id,
        "datum": iso_datum,
    }
    async with aiohttp.request("POST", url, data=data) as resp:
        html = await resp.text()
    paths = []
    txt_paths = PATHS_RE.findall(html)
    for path_txt in txt_paths:
        points = [(float(c1), float(c2)) for c1, c2 in COORDS_RE.findall(path_txt)]
        paths.append(points)

    stops = _get_stops(html)

    return LineInfo(
        id=linija_id,
        stops=stops,
        paths=paths
    )


class MarpromWebShop:
    BASE_URL = 'https://marpromprodaja.azurewebsites.net/api'
    headers = {
        'authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ilg1ZVhrNHh5b2pORnVtMWtsMll0djhkbE5QNC1jNTdkTzZRR1RWQndhTmsifQ.eyJpc3MiOiJodHRwczovL21hcnByb21mYS5iMmNsb2dpbi5jb20vOGE0YjRiZTUtM2U3Ny00ODFiLTkyN2ItODdhYTRiMzk3M2M1L3YyLjAvIiwiZXhwIjoxNjczMjY1MjgxLCJuYmYiOjE2NzMyNjE2ODEsImF1ZCI6IjJkZDUxZWMyLWI4N2UtNGE2MC04ODE2LWQ5YzQ4ZGVlYjUyMyIsInN1YiI6ImY4OTRjMzU2LWEwZjQtNDlkZC05MWZlLTY5MjE0NTU0ODg4MyIsImdpdmVuX25hbWUiOiJSb2JvdCIsImZhbWlseV9uYW1lIjoiUm9ib3RuaWsiLCJleHRlbnNpb25fUGhvbmUiOiIwNjg5MjU0MjAiLCJleHRlbnNpb25fQWdyZW…ImExNzRhNzdmLTY1NDgtNDQyOC1hZjJhLTJjZTNkNGRhZmE1MiIsInNjcCI6ImJhc2UiLCJhenAiOiIyZGQ1MWVjMi1iODdlLTRhNjAtODgxNi1kOWM0OGRlZWI1MjMiLCJ2ZXIiOiIxLjAiLCJpYXQiOjE2NzMyNjE2ODF9.crvL7577gWJ0ctjSohsnhotQaRsYi5uDF0LiHuAT6QKgiidiF-ee_8x1G9yGogp5CBR2TgclcXR8ofaaGOS-A5lpnJXamMsKFpCPpR7EFGyFigAW5E-H2JDWxptu9zth92XRk2mg2XLD7RkvsPV6dJDzkF8YAr3MvD-495Qt4JnNSgGouHH_J0Bboq5Vmla-ZQpCvkVE7hF2vicD-PeB_a8XtBI0PyqiM8yiZbdeH-Dy9OhK6Z9B1AP20JiautGD5z_t8gHenj1u5IgHQyvnZPVDhGCTDzMA0YTVGDE5rjaUdQIzxMPC5B9mQhGyHM0Dy7re8x07rve6BV61o44VnA',
    }

    async def _request(self, path, method='GET', json=None):
        assert path[0] == '/'
        async with aiohttp.request(method, self.BASE_URL + path, json=json, headers=self.headers) as resp:
            return await resp.json()

    async def login(self, email, password):
        async with aiohttp.request('POST', self.BASE_URL + '/login', json={
            'email': email,
            'password': password,
        }) as resp:
            return await resp.json()

    async def list_card_products(self, cuid):
        return await self._request('/ListCardProducts/', json={
            'cuid': cuid,
        })


if __name__ == '__main__':
    # stops = seznam_postaj()
    # print(stops)
    async def main():
        shop = MarpromWebShop()
        # await shop.login()
        r = await shop.list_card_products('36132326976473092')
        print(r)

    asyncio.run(main())
