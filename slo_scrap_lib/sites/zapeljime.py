import asyncio
import json

import aiohttp
from geojson_pydantic import FeatureCollection, Point, Feature

from slo_scrap_lib.models import RideshareProvider, RidesharePointType, RideshareStation, RideshareVehicle


async def get_stations():
    data = {
        'get_set_what_content': 'get_seznam',
        'zeljeno_kolo': '1',
    }
    async with aiohttp.request("POST", 'https://zapelji.me/api/_service_json_elby.php', data=data) as resp:
        text = await resp.text()
        data = json.loads(text)

    features = []
    for bike in data['kolesa']:
        if bike['deluje'] != '1' or bike['rezerviran'] != '0':
            continue

        point = Point(coordinates=(float(bike['lokacija_longitude']), float(bike['lokacija_latitude'])))
        feat = Feature(
            id=bike['kolo_id'],
            geometry=point,
            properties=RideshareVehicle(
                title=bike['tablica_nr'],
                id=bike['kolo_id'],
                price_start=bike['cena_rezervacija'],
                image_url=bike['last_image_url'],
                type=RidesharePointType.BIKE_FLOATING,
                provider=RideshareProvider.ZAPELJI_ME,
                system='zapelji_me',
            ).dict()
        )
        features.append(feat)

    return FeatureCollection(features=features)


if __name__ == '__main__':
    async def main():
        ft = await get_stations()
        print(ft)

    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
