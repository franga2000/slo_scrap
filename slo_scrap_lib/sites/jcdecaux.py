import asyncio

import aiohttp
from bs4 import BeautifulSoup
import re
from geojson_pydantic import FeatureCollection, Point, Feature

from slo_scrap_lib.models import RideshareProvider, RidesharePointType, RideshareStation, RideshareVehicle

API_KEY = "frifk0jbxfefqqniqez09tw4jvk37wyf823b5j1i"


async def get_stations(contract):
    url = "https://api.jcdecaux.com/vls/v3/stations"
    params = {
        "apiKey": API_KEY,
        "contract": contract,
    }

    async with aiohttp.request("GET", url, params=params) as resp:
        data = await resp.json()

    features = []
    for bike in data:
        if bike['status'] == 'OPEN':
            point = Point(coordinates=(bike['position']['longitude'], bike['position']['latitude']))
            feat = Feature(
                id=bike['number'],
                geometry=point,
                properties=RideshareStation(
                    title=bike['name'],
                    id=bike['number'],
                    type=RidesharePointType.BIKE_STATION,
                    provider=RideshareProvider.EUROPLAKAT,
                    system=bike['contractName'],
                    capacity=bike['totalStands']['capacity'],
                    capacity_free=bike['totalStands']['availabilities']['stands'],
                    vehicles_available=bike['totalStands']['availabilities']['bikes'],
                ).dict()
            )
            features.append(feat)

    return FeatureCollection(features=features)


EUROPLAKAT_ENDPOINTS = {
    'maribor': 'https://www.mbajk.si/',
    'ljubljana': 'https://www.bicikelj.si/'
}
EUROPLAKAT_SCRIPT_REGEX = r"clientKey:\"(?P<key>\w+)\",clientCode:\"(?P<code>[^\"]+)\""

headers = {
    'maribor': {
        'Accept': 'application/vnd.bikes.v3+json',
        'Authorization': '',
    },
    'ljubljana': {
        'Accept': 'application/vnd.bikes.v3+json',
        'Authorization': '',
    }
}


async def _authenticate(contract: str = 'ljubljana'):
    global headers
    session = aiohttp.ClientSession()

    site_resp = await session.get(f"{EUROPLAKAT_ENDPOINTS[contract]}sl/mapping")
    site_resp.raise_for_status()
    site_soup = BeautifulSoup(await site_resp.text())
    script_src = site_soup.find_all("script")[-1].get("src")

    script_resp = await session.get(f"{EUROPLAKAT_ENDPOINTS[contract]}{script_src}")
    script_resp.raise_for_status()
    matches = re.findall(EUROPLAKAT_SCRIPT_REGEX, await script_resp.text(), re.MULTILINE)

    tokens_resp = await session.post("https://api.cyclocity.fr/auth/environments/PRD/client_tokens", json={
        "code": matches[0][1],
        "key": matches[0][0]
    })
    tokens_resp.raise_for_status()
    tokens = await tokens_resp.json()

    auth_resp = await session.post('https://api.cyclocity.fr/auth/access_tokens',
                             json={'refreshToken': tokens['refreshToken']})
    auth_resp.raise_for_status()
    data = await auth_resp.json()
    headers[contract]['Authorization'] = f"Taknv1 {data['accessToken']}"

    await session.close()


async def _authenticated_request(contract: str = 'ljubljana', inner=False, **kwargs):
    global headers
    try:
        assert headers[contract]['Authorization']
        async with aiohttp.request(**kwargs, headers=headers[contract]) as resp:
            resp.raise_for_status()
            return await resp.json()
    except Exception:
        if inner:
            return
        await _authenticate(contract=contract)
        return await _authenticated_request(**kwargs, contract=contract, inner=True)


async def get_vehicles(contract, station_id):
    contract = contract.lower()
    url = 'https://api.cyclocity.fr/contracts/' + contract + '/bikes'
    station = await _authenticated_request(contract=contract, method='GET', url=url,
                                           params={'stationNumber': station_id})

    bikes = []
    for item in station:
        if item['status'] != 'AVAILABLE':
            continue
        bike = RideshareVehicle(
            id=item['id'],
            model=item['type'],
            title=str(item['number']),
            number_in_station=item['standNumber'],
            rating=item['rating']['value'] if item['rating'] else None,
            ratings=item['rating']['count'] if item['rating'] else None
        )
        bikes.append(bike)

    bikes.sort(key=lambda b: b.number_in_station)
    return bikes


if __name__ == '__main__':
    ft = asyncio.get_event_loop().run_until_complete(get_vehicles('ljubljana', 12))
    print(ft)
