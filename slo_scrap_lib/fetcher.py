import logging
from datetime import timedelta
from itertools import cycle
from os import PathLike
from typing import Set, Dict

import requests
from aiohttp import ClientSession
from aiohttp.client import _SessionRequestContextManager
from requests import Response, Request, PreparedRequest
from requests.exceptions import ProxyError

_session = None


async def session():
    global _session
    if _session is None or _session.closed:
        _session = ClientSession()
    return _session

class InvalidResponse(Exception):
    def __init__(self, response: Response, tries):
        super().__init__(f"Response invalid after {tries} attempts", response)


class BaseFetcher:
    ACCEPT_CODES: Set[int] = {200, }
    USER_AGENT: str = "Mozilla/5.0 (Android 4.4; Mobile; rv:41.0) Gecko/41.0 Firefox/41.0"
    DEFAULT_HEADERS: Dict[str, str] = {
        "User-Agent": USER_AGENT,
    }

    def __init__(self):
        # Prepare session
        self.session = requests.Session()
        self.session.headers = self.DEFAULT_HEADERS

    def is_valid(self, response: Response) -> bool:
        return response.status_code < 400 or response.status_code in self.ACCEPT_CODES

    def ensure_request(self, request: Request, **kwargs) -> Response:
        prepared_request: PreparedRequest = self.session.prepare_request(request)
        try:
            response = self.session.send(prepared_request, **kwargs)
        except requests.exceptions.RequestException:
            logging.exception("Requests exception")
        else:
            if self.is_valid(response):
                return response
            else:
                raise InvalidResponse(response, 1)

class AutoProxyFetcher(BaseFetcher):
    RETRIES: int = 3
    PROXY_FILE: PathLike = "proxy_list.txt"

    def __init__(self):
        super().__init__()

        # Load proxies
        with open(self.PROXY_FILE) as f:
            self.proxies = set((line.strip() for line in f.readlines() if not line.startswith("#")))
        self.proxy_rotator = cycle(self.proxies)
        self.set_proxy(next(self.proxy_rotator))

    def ensure_request(self, request: Request, **kwargs) -> Response:
        response = None
        for i in range(self.RETRIES+1):
            prepared_request: PreparedRequest = self.session.prepare_request(request)
            settings = self.session.merge_environment_settings(request.url, {}, self.session.proxies, False, None)
            try:
                response = self.session.send(prepared_request, **dict(settings, **kwargs))
            except ProxyError:
                logging.exception("Proxy might be down:" + self.session.proxies["http"])
            except requests.exceptions.RequestException:
                logging.exception("Requests exception (using proxy " + self.session.proxies["http"] + ")")
            else:
                if self.is_valid(response):
                    return response
            logging.error(f"Failed with {self.session.proxies}: {response}")
            self.rotate_proxy()
        raise InvalidResponse(response, self.RETRIES+1)

    def rotate_proxy(self):
        self.set_proxy(next(self.proxy_rotator))

    def set_proxy(self, proxy):
        if proxy:
            self.session.proxies = {
                "http": proxy,
                "https": proxy
            }
        else:
            self.session.proxies = None


class CachedFetcher(BaseFetcher):
    def __init__(self):
        super().__init__()
        import requests_cache
        self.session = requests_cache.CachedSession(
            'request_cache',
            use_cache_dir=True,  # Save files in the default user cache dir
            cache_control=False,  # Use Cache-Control headers for expiration, if available
            expire_after=timedelta(days=1),  # Otherwise expire responses after one day
            allowable_methods=['GET', 'POST'],  # Cache POST requests to avoid sending the same data twice
            allowable_codes=[200, 400],  # Cache 400 responses as a solemn reminder of your failures
            ignored_parameters=['api_key'],  # Don't match this param or save it in the cache
            match_headers=True,  # Match all request headers
            stale_if_error=True,  # In case of request errors, use stale cache data if possible
        )